import { IApplicationState } from '../store'

import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from 'react-redux'

export const useState: TypedUseSelectorHook<IApplicationState> = useReduxSelector
