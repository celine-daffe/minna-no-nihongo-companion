export const enum ErrorAction {
  SET_ERROR = 'SET_ERROR',
}

export interface IErrorAction {
  type: ErrorAction
  payload: string
}
