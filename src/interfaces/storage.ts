import { IQuestionResultFeedback } from '.'

export interface IDBResultItem {
  time: number
  correct: boolean,
  submittedAnswer: string[]
}

export interface IDBResult {
  question_id: string
  results: IDBResultItem[]
}

export interface ICollatedFeedback {
  result: IDBResultItem
  feedback: IQuestionResultFeedback
}