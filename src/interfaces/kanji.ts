export interface IHiraganaMatch {
  hiragana: string,
  replace: string
  kanji: string
  add: string
}

export interface IKanjiMatch {
  kanji: string,
  ruby: Array<{
    kanji: string
    hiragana: string,
  }>
}
