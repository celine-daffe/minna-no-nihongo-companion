import { ActivityType, IQuestionResultFeedback } from '.'

export interface IConversationItem {
  source?: string
  line: string
}

export interface IGrammarItem {
  readonly id: string
  readonly ruleId: number
  hiragana: string | IConversationItem[]
  replaced?: string[],
  answer?: string[],
  alsoAllow?: string[]
  english?: string | IConversationItem[],
  restrictToActivity?: ActivityType,
  feedback?: IQuestionResultFeedback[]
}

export interface IGrammarRule {
  readonly id: number
  particles: string[]
  rule: string
  explanation: string,
  decoys?: string[]
}

export const enum Noun {
  TOBACCO,
  BOOK,
  DICTIONARY,
  MAGAZINE,
  NEWSPAPER,
  ALCAHOL,
  PHOTO,
  PASSPORT,
  RAMEN,
  YAKISOBA
}

export interface IVocabularyItem {
  hiragana: string
  english: string
}

export interface INoun extends IVocabularyItem {
  key: Noun
  chapter: number,
  type?: 'singular' | 'plural'
}

export interface IVerbOption {
  readonly verbId: string,
  availableNouns: Noun[]
}
