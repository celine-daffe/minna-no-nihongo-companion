import {
  IQuestion,
  IOptionItem,
  IActivityOption,
  IQuestionEvaluationResult,
  IQuestionResultFeedback,
  INumQuestionOption,
  OptionPair
} from '.'

export enum ActivityType {
  NONE,
  TEST_GRAMMAR,
  TEST_ADJECTIVES,
  TEST_VERB_CONJUGATION,
  SENTENCE_TESTER,
  DICT_ADJECTIVES,
  DICT_VERBS
}

export enum CategoryType {
  NONE,
  GRAMMAR,
  VERBS,
  ADJECTIVES,
}

export const enum ActivityAction {
  SET_CATEGORY = 'SET_CATEGORY',
  SET_ACTIVITY = 'SET_ACTIVITY',
  SET_OPTION = 'SET_OPTION',
  START = 'START',
  COMPLETE = 'COMPLETE',
  CONFIGURE = 'CONFIGURE',
  NEXT_QUESTION = 'NEXT_QUESTION',
  SKIP_QUESTION = 'SKIP_QUESTION',
  UNMASK_QUESTION = 'UNMASK_QUESTION',
  SUBMIT_ANSWER = 'SUBMIT_ANSWER',
  RECORD_RESULT = 'RECORD_RESULT',
  SHOW_SUMMARY = 'SHOW_SUMMARY',
  RESET = 'RESET'
}

export interface IActivityAction {
  type: ActivityAction
  payload?: ActivityType | IActivity | IQuestion | IOptionItem | string[] | null
}

export interface IActivity {
  type: ActivityType
  title: string
  subtitle: string
}

export interface IQuizActivity extends IActivity {
  options: IActivityOption[] | OptionPair
  evaluateAnswer: (answer: any, question: IQuestion) => IQuestionEvaluationResult
  getQuestions: (
    options?: INumQuestionOption
  ) => Iterator<IQuestion | never>
  renderQuestion: (question: IQuestion) => string
  getFeedback: (question: IQuestion, submittedAnswer: string[]) => IQuestionResultFeedback | null
}
