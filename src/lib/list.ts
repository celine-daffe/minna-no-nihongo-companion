import { Adjective } from 'minna-no-nihongo-adjectives'
import { Verb } from 'minna-no-nihongo-verbs'

export const filterCollectionByStringOrChapter = (
  collection: Verb[],
  value: string
): Verb[] => {
  const chapter = parseInt(value, 10)
  const { isNaN, isInteger } = Number

  if (isNaN(chapter)) {
    return collection.filter((i: Verb) =>
      i.english.startsWith(value)
      || i.polite_form.present.startsWith(value)
      || i.plain_form.present.startsWith(value)
    )
  }

  if (isInteger(chapter)) {
    return chapter < 26
      ? collection.filter(i => i.chapter === chapter)
      : []
  }

  return collection
}

export const filterAdjectiveCollectionByStringOrChapter = (
  collection: Adjective[],
  value: string
): Adjective[] => {
  const chapter = parseInt(value, 10)
  const { isNaN, isInteger } = Number

  if (isNaN(chapter)) {
    return collection.filter((i: Adjective) =>
      i.english.startsWith(value)
      || i.hiragana.startsWith(value)
    )
  }

  if (isInteger(chapter)) {
    return chapter < 26
      ? collection.filter(i => i.chapter === chapter)
      : []
  }

  return collection
}