import adjectives from 'minna-no-nihongo-adjectives'
import { shuffle } from '../utils'
import {
  IAdjectiveTesterOptions,
  IAdjectiveTesterQuestion,
  IQuestionCollection
} from '../interfaces'

export const getAdjectiveQuestions = (
  options: IAdjectiveTesterOptions
): IQuestionCollection<IAdjectiveTesterQuestion> => {
  const nums = new Set<number>()

  while (nums.size !== options.NUM_QUESTIONS) {
    nums.add(Math.floor(Math.random() * adjectives.length - 1) + 1)
  }

  return {
    questions: [...nums].map((num: number) =>
      getAdjectiveQuestionItem(num)
    )
  }
}

const getAdjectiveQuestionItem = (
  index: number
): IAdjectiveTesterQuestion => {
  const item = adjectives[index]
  const answer: string = item.english

  // Grab a set of options, and include he correct answer(s)
  const availableAdjectives = adjectives.filter(a => a.english !== answer)
  const answerOptions = new Set<string>()

  while (answerOptions.size !== 3) {
    const answerOption = availableAdjectives[Math.floor(Math.random() * availableAdjectives.length)]
    answerOptions.add(answerOption.english)
  }

  const options = shuffle([
    ...answerOptions,
    answer
  ])

  return {
    id: item.id,
    question: item.hiragana,
    answer: [answer],
    options,
    meta: {
      english: item.english,
      kanji: item.kanji
    }
  }
}
