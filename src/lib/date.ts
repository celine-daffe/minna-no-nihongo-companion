import formatter from '@uiw/formatter'

const FORMAT_SHORT = 'MM/DD HH:MM'

export const formatDateShort = (date: Date): string => {
  return formatter(FORMAT_SHORT, date)
}
