export const ratePerformance = (
  numberOfQuestions: number,
  numberOfcorrectAnswers: number
) => {
  const percentage = numberOfcorrectAnswers / numberOfQuestions
  return `${performancePrefix(percentage)} You scored ${Math.round(percentage * 100)}%`
}

const performancePrefix = (percentage: number) => {
  if (percentage === 1) { return 'Excellent!' }
  if (percentage >= 0.8) { return 'Nice.' }
  if (percentage >= 0.6) { return 'Not Bad...' }
  if (percentage >= 0.4) { return 'Hmmm...' }
  if (percentage >= 0.2) { return 'Bummer...' }

  return 'Oh dear...'
}