import {
  particles,
  grammarItems,
  grammarRules
} from '../data'

import { maskGrammarInRuleItem } from './'
import {
  equalArrays,
  filterByChapterRange,
  padArrayWith,
} from '../utils'

import {
  IGrammarItem,
  IGrammarRule,
  IGrammarTesterQuestion,
  IGrammarTesterOptions,
  IQuestion,
  IQuestionCollection,
  IQuestionEvaluationResult,
} from '../interfaces'

// how many particles are made available as options for each question
export const NUMBER_OF_GRAMMAR_ITEM_OPTIONS: number = 8

export const nextParticles = (grammarItem: IGrammarItem): string[] => {
  if (grammarItem.answer) {
    return padArrayWith(grammarItem.answer, particles, NUMBER_OF_GRAMMAR_ITEM_OPTIONS)
  }

  const rule = getRuleById(grammarItem.ruleId)

  if (!rule) {
    console.warn(grammarItem) // tslint:disable-line
    throw Error(`Unable to find rule for grammar item with ruleId '${grammarItem.ruleId}'`)
  }

  return padArrayWith(rule.particles, particles, NUMBER_OF_GRAMMAR_ITEM_OPTIONS)
}

export const getRuleById = (id: number): IGrammarRule =>
  grammarRules.find(r => r.id === id) as IGrammarRule

export const getGrammarQuizQuestions = (
  options: IGrammarTesterOptions
): IQuestionCollection<IGrammarTesterQuestion> => {
  let result: IQuestionCollection<IGrammarTesterQuestion> = {
    questions: []
  }

  const availableGrammarItems = filterByChapterRange(grammarItems, options)

  if (!availableGrammarItems.length) {
    return {
      questions: [],
      message: 'No items were found which match the filters provided'
    }
  }

  if (availableGrammarItems.length < options.NUM_QUESTIONS) {
    options.NUM_QUESTIONS = availableGrammarItems.length

    result = {
      ...result,
      message: `Only ${availableGrammarItems.length} items were found matching the given filters.`
    }
  }

  const nums = new Set<number>()

  while (nums.size !== parseInt((options.NUM_QUESTIONS as unknown) as string, 10)) {
    nums.add(Math.floor(Math.random() * availableGrammarItems.length - 1) + 1)
  }

  return {
    ...result,
    questions: [...nums].map((num: number) =>
      getQuestionItem(availableGrammarItems, num)
    )
  }
}

const getQuestionItem = (
  items: IGrammarItem[],
  index: number
): IGrammarTesterQuestion => {
  const item = items[index]

  const rule = getRuleById(item.ruleId)
  const particleOptions = nextParticles(item)
  const answer = item.answer ? item.answer : rule.particles

  return {
    id: item.id,
    question: maskGrammarInRuleItem(item),
    answer,
    options: particleOptions,
    meta: {
      ruleId: item.ruleId
    }
  }
}

export const checkAnswer = (
  submittedParticles: string[],
  question: IQuestion
): IQuestionEvaluationResult => {
  if (question.answer.length > submittedParticles.length) {
    return { correct: null }
  }

  return {
    correct: equalArrays(submittedParticles, question.answer)
  }
}
