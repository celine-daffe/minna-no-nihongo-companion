import { mergeObjects, isNumber } from '../utils'
import { ActivityType, IDBResult, IDBResultItem, IOptionItem, IQuestionResult } from '../interfaces'
import { IOptionState } from '../store/reducers/activity'
import { activities as availableActivites } from '../data'
import * as indexeddb from 'indexeddb'

const numQuestionDefault = {
  NUM_QUESTIONS: 10
}

// @TODO This is dodgy, should be collecting the default values from each activity
const defaultOptions = () => availableActivites.reduce((
  accumulator,
  currentValue,
) => {
  return mergeObjects(accumulator, {
    [currentValue.type.toString()]: numQuestionDefault
  })
}, {})

export const persistOption = (payload: IOptionItem) => {
  const { activityId, optionKey, optionValue } = payload

  return localStorage.setItem(`${activityId}_${optionKey}`, optionValue)
}

export const loadOptions = (): IOptionState => {
  const result = groupByActivity(
    Object.entries(localStorage)
  )

  return mergeObjects(
    defaultOptions(),
    result
  )
}

export const getOptionsForActivity = (optionsData: IOptionState, activityId: ActivityType) => {
  return optionsData[activityId]
}

const groupByActivity = (input: string[][]) => {
  let activities: Record<string, any> = {}

  input.map((i) => {
    const [key, value] = i
    const activity: ActivityType = parseInt(key.slice(0, 1), 10)
    const item = {
      [key.substring(2, key.length)]: isNumber(value) ? parseInt(value, 10) : value
    }

    activities = {
      ...activities,
      [activity]: {
        ...activities[activity],
        ...item
      }
    }
  })

  return activities
}

const getResultsStore = () => {
  return (indexeddb.default)('minna-no-db', {
    version: 1
  }).store('results', {
    key: 'question_id',
    indexes: []
  })
}

export const persistResult = (result: IQuestionResult) => {
  const store = getResultsStore()
  const { id } = result.question

  const resultItem: IDBResultItem = {
    time: Date.now(),
    correct: result.correct,
    submittedAnswer: result.submittedAnswer
  }

  store.get(id, (error, record) => {
    if (error) {
      // tslint:disable-next-line: no-console
      console.error(error)
    }

    if (record) {
      store.update({ question_id: id, results: [...record.results, resultItem] })
    } else {
      store.add({ question_id: id, results: [resultItem] })
    }
  })
}

export const getQuestionHistory = async (questionId: string): Promise<IDBResult> => {
  return await getResultsStore().get(questionId) as IDBResult
}
