import { IConversationItem, IGrammarItem } from '../interfaces'

// The characters which appear in place of the particle
export const REPLACEMENT_CHARS: string = '<span>?</span>'

export const RULE_ITEM_TYPE_SENTENCE = 1
export const RULE_ITEM_TYPE_CONVERSATION = 2

export const getRuleItemType = (ruleItem: IGrammarItem): number => {
  if (ruleItem.hiragana  instanceof Array) {
    return RULE_ITEM_TYPE_CONVERSATION
  }

  return RULE_ITEM_TYPE_SENTENCE
}

const matchGrammarItems = (sentence: string) => sentence.match(/{.*?}/g)

/**
 * The following methods mask and unmask particles for display purposes
 */
export interface IMaskedRuleItem extends IGrammarItem { masked?: true }
export interface IUnmaskedRuleItem extends IGrammarItem { masked?: false }

export const maskGrammarInRuleItem = (ruleItem: IGrammarItem): IGrammarItem => {
  switch (getRuleItemType(ruleItem)) {
    case RULE_ITEM_TYPE_CONVERSATION:
      return maskGrammarInConversation(ruleItem)
    case RULE_ITEM_TYPE_SENTENCE:
    default:
      return {
        ...ruleItem,
        hiragana: maskGrammarInSentence(ruleItem.hiragana  as string)
      }
  }
}

const maskGrammarInConversation = (ruleItem: IGrammarItem): IMaskedRuleItem => {
  return {
    ...ruleItem,
    hiragana: (ruleItem.hiragana  as IConversationItem[])
      .map((content: IConversationItem) => ({
        source: content.source,
        line: maskGrammarInSentence(content.line)
      }))
  }
}

const maskGrammarInSentence = (sentence: string): string => {
  const grammarInSentence = matchGrammarItems(sentence)

  if (!grammarInSentence) { return sentence }

  return sentence.split(' ').map((group: string) => {
    return (grammarInSentence.indexOf(group) > -1)
      ? REPLACEMENT_CHARS
      : group
  }).join(' ')
}

export const unmaskGrammarInRuleItem = (particles: string[], ruleItem: IGrammarItem): IUnmaskedRuleItem => {
  if( typeof particles === 'string'){
    particles = [particles]
  }
  
  if (ruleItem.replaced && ruleItem.replaced.length) {
    particles = particles.filter((p: string) => !(ruleItem.replaced as string[]).includes(p))
  }

  switch (getRuleItemType(ruleItem)) {
    case RULE_ITEM_TYPE_CONVERSATION:
      return unmaskGrammarInConversation(particles, ruleItem)
    case RULE_ITEM_TYPE_SENTENCE:
    default:
      return {
        ...ruleItem,
        hiragana: unmaskGrammarInSentence(particles, ruleItem.hiragana  as string),
        replaced: particles
      }
  }
}

const unmaskGrammarInConversation = (particles: string[], ruleItem: IGrammarItem): IUnmaskedRuleItem => {
  return {
    ...ruleItem,
    hiragana: (ruleItem.hiragana  as IConversationItem[])
      .map((content: IConversationItem) => ({
        source: content.source,
        line: unmaskGrammarInSentence(particles, content.line),
      })),
    replaced: particles
  }
}

const unmaskGrammarInSentence = (particles: string[], sentence: string): string => {
  const segmentsToBeReplaced = sentence.split(' ').filter(s => s === REPLACEMENT_CHARS)

  if (!particles.length || !segmentsToBeReplaced.length) {
    return sentence
  }

  return sentence.replace(REPLACEMENT_CHARS, particles[0])
}
