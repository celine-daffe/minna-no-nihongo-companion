import { IDBResult, ICollatedFeedback, IQuestionResultFeedback } from '../interfaces'
import {
  getSentenceQuestionFeedback,
  getSentenceQuestionItem,
  getSentenceById
} from './sentence'

const unique = (items: ICollatedFeedback[]) =>
  items.reduce((collection: ICollatedFeedback[], item) => {
    return collection.find(existingItem => existingItem.feedback.advice === item.feedback.advice)
      ? collection
      : [...collection, item]
  }, [] as ICollatedFeedback[])

export const collateFeedback = (item: IDBResult): ICollatedFeedback[] => {
  const incorrectResults = item.results.filter(i => !i.correct)

  if (!incorrectResults.length) {
    return []
  }

  const question = getSentenceQuestionItem(
    0,
    [getSentenceById(item.question_id)]
  )

  return unique(incorrectResults.map(result =>
    ({
      result,
      feedback: getSentenceQuestionFeedback(
        question,
        result.submittedAnswer.join('')
      ) as IQuestionResultFeedback
    })
  )
    .filter(i => i)
    .filter(i => i.feedback)) as ICollatedFeedback[]
}
