import * as React from 'react'
import { Adjective } from 'minna-no-nihongo-adjectives'
import Panel, { PanelHeading, PanelDivider, PanelActions } from './Panel'
import Button from './Button'
import { withRuby } from '../lib'

enum AdjectiveType {
  'い',
  'な'
}

const AdjectiveItem: React.FC<{
  adjective: Adjective,
  destroy: () => void
}> = ({ adjective, destroy }) => {
  const {
    hiragana,
    english,
    chapter,
    adjective_type,
    past,
    past_negative,
    negative
  } = adjective

  return (
    <Panel>
      <PanelHeading
        heading={hiragana + ' / ' + english}
        subheading={
          `Ch. ${chapter} / ${AdjectiveType[adjective_type]} -adjective`
        }
      />
      <PanelDivider />
      <h1 dangerouslySetInnerHTML={{ __html: withRuby(adjective.kanji) }} />
      <p><strong>Plain</strong>: {hiragana}</p>
      <p><strong>Plain (Past)</strong>: {past}</p>
      <p><strong>Negative-Form</strong>: {negative}</p>
      <p><strong>Negative-Form (Past)</strong>: {past_negative}</p>
      <PanelDivider />
      <PanelActions>
        <Button
          label="Close"
          action={() => destroy()}
        />
      </PanelActions>
    </Panel>
  )
}

export default AdjectiveItem