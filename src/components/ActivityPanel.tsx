import * as React from 'react'
import Button from './Button'
import Panel, { PanelHeading, PanelDivider, PanelActions } from './Panel'

interface IAction {
  label: string
  action: () => any
}

interface IActivityPanelProps {
  heading: string
  subheading: string
  actions: IAction[]
}

const ActivityPanel: React.FC<IActivityPanelProps> = ({ heading, subheading, actions }) => {
  return (
    <Panel>
      <PanelHeading heading={heading} subheading={subheading} />
      <PanelDivider />
      <PanelActions>
        {
          actions.map((action, index) => (
            <Button key={`action-${index}`} {...action} />
          ))
        }
      </PanelActions>
    </Panel>
  )
}

export default ActivityPanel