import * as React from 'react'
import { useDispatch } from 'react-redux'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../Panel'
import AdjectiveList from './List'
import AdjectiveListFilter from './ListFilter'
import Button from '../Button'
import { useState } from '../../hooks'
import { setAdjectives, resetAdjectives } from '../../store/actions/list'
import { backToMenu } from '../../store/actions/activity'
import { filterAdjectiveCollectionByStringOrChapter } from '../../lib'

const AdjectiveDictionary: React.FC<{}> = () => {
  const dispatch = useDispatch()
  const state = useState(s => s)
  const { list } = state

  const close = () => {
    dispatch(
      backToMenu()
    )
  }
  
  const filterAdjectives = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = event.target

    if (value && value.length) {
      dispatch(
        setAdjectives(
          filterAdjectiveCollectionByStringOrChapter(list.adjectives, value)
        )
      )
    } else {
      dispatch(resetAdjectives())
    }
  }

  return (
    <Panel>
      <PanelHeading
        heading='Adjective list'
        subheading='Filter by english, hiragana, or chapter number'
      />
      <PanelDivider />
      <PanelActions>
        <Button label="Back to Menu" action={() => close()}/>
      </PanelActions>
      <PanelDivider />
      <AdjectiveListFilter onChange={filterAdjectives} />
      <PanelDivider />
      <AdjectiveList adjectives={list.filteredAdjectives} />
    </Panel>
  )
}

export default AdjectiveDictionary
