import styled from '@emotion/styled'
import { theme } from '../../data'

const Table = styled.table`
  width: 100%;
  font-family: ${theme.fontFamily};
  font-size: ${theme.fontSize.slightlySmaller};
  font-weight: lighter;

  tr:nth-of-type(odd) {
    background-color: #efefef;
  }

  td {
    padding: 0.5rem;
    width: 50%;
  }

  th, td {
    text-align: left
  }

  th + th, td + td {
    text-align: right;
  }
`

export default Table