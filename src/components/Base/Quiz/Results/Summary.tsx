import * as React from 'react'
import { useDispatch } from 'react-redux'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../../../Panel'
import Button from '../../../Button'
import { restartActivity, quitActivity } from '../../../../store/actions/activity'
import { ratePerformance } from '../../../../lib'
import { IQuestionResult } from '../../../../interfaces'

const Summary: React.FC<{ results: IQuestionResult[] }> = ({ results }) => {
  const dispatch = useDispatch()
  const numberOfCorrectAnswers = results.filter((result) =>
    result.correct
  ).length

  return (
    <Panel>
      <PanelHeading
        heading={ratePerformance(
          results.length,
          numberOfCorrectAnswers
        )}
      />
      <PanelDivider />
      <PanelActions>
        <Button
          label="Quit"
          action={() => dispatch(quitActivity())}
        />
        <Button
          label="Retry"
          action={() => dispatch(restartActivity())}
        />
      </PanelActions>
    </Panel>
  )
}

export default Summary