import * as React from 'react'
import { IQuestionResult } from '../../../../interfaces'
import { IActivityState } from '../../../../store/reducers/activity'
import Result from './Result'
import Summary from './Summary'

const Results: React.FC<{ activity: IActivityState }> = ({ activity }) => {
  const { results, currentActivity } = activity

  return (
    <React.Fragment>
      <Summary results={results} />
      {
        activity.results
          .map((result: IQuestionResult) =>
            <Result
              currentActivity={currentActivity}
              key={result.question.id}
              result={result}
            />
          )
      }
    </React.Fragment>
  )
}

export default Results
