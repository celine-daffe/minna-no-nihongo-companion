import * as React from 'react'
import Panel, { PanelHeading } from '../../../Panel'
import { IQuestionResult, IQuizActivity } from '../../../../interfaces'
import { theme } from '../../../../data'
import Feedback from './Feedback'
import { matchSpacing } from '../../../../lib'

// @TODO, "It looks like you need to work on..."
// For advice, return an error type (vocab.verb, verb.teform) and steer towards practicing that

import styled from '@emotion/styled'

const ResultSummaryContainer = styled.div`
  p {
    margin: 0.6rem 0;
    font-size: ${theme.fontSize.slightlySmaller};
    font-weight: lighter;
  }

  p strong {
    font-weight: bold;
  }
`

const ResultSummary: React.FC<{
  correct: boolean,
  correctAnswer: string,
  submittedAnswer: string,
}> = ({
  correct,
  correctAnswer,
  submittedAnswer,
}) => {
    return (
      <ResultSummaryContainer>
        {
          correct
            ? <p>You correctly answered: {correctAnswer}</p>
            : (
              <React.Fragment>
                <p><strong>Correct answer:</strong></p>
                <p>{correctAnswer}</p>
                <p><strong>Your answer:</strong></p>
                <p>{matchSpacing(submittedAnswer, correctAnswer)}</p>
              </React.Fragment>
            )
        }
      </ResultSummaryContainer>
    )
  }

const Result: React.FC<{
  result: IQuestionResult,
  currentActivity: IQuizActivity
}> = ({ result, currentActivity }) => {
  const { question, submittedAnswer, correct, feedback } = result
  const { answer } = question

  return (
    <Panel
      borderTop={correct
        ? theme.colours.green.dark
        : theme.colours.red.dark}
    >
      <PanelHeading
        heading={currentActivity.renderQuestion(question)}
      />
      <ResultSummary
        correct={correct}
        correctAnswer={answer.join('')}
        submittedAnswer={submittedAnswer.join('')}
      />
      <Feedback feedback={feedback} submittedAnswer={submittedAnswer} />
    </Panel>
  )
}

export default Result
