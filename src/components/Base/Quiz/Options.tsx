import * as React from 'react'
import Panel, { PanelHeading, PanelDivider } from '../../Panel'
import Option from './Option'
import { IActivityState } from '../../../store/reducers/activity'
import { IActivityOption } from '../../../interfaces'

const Options: React.FC<{ activity: IActivityState }> = ({ activity }) => {
  const { currentActivity, options } = activity
  const { options: currentActivityOptions } = currentActivity

  return !currentActivityOptions ? null : (
    <Panel>
      <PanelHeading
        heading={'Options'}
      />
      <PanelDivider />
      {(currentActivityOptions as IActivityOption[]).map(o =>
        <Option
          key={o.id}
          activityId={currentActivity.type}
          option={o}
          options={options}
        />
      )}
    </Panel>
  )
}

export default Options
