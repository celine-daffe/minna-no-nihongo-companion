import * as React from 'react'
import { useDispatch } from 'react-redux'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../../Panel'
import Button from '../../Button'
import { startActivity, quitActivity } from '../../../store/actions/activity'
import { IActivityState } from '../../../store/reducers/activity'

const Intro: React.FC<{ activity: IActivityState }> = ({ activity }) => {
  const dispatch = useDispatch()
  const { currentActivity } = activity

  return (
    <Panel>
      <PanelHeading
        heading={currentActivity.title}
        subheading={currentActivity.subtitle}
      />
      <PanelDivider />
      <PanelActions>
        <Button
          label="Back"
          action={() => dispatch(quitActivity())}
        />
        <Button
          label="Start"
          action={() => dispatch(startActivity())}
        />
      </PanelActions>
    </Panel>
  )
}

export default Intro
