import * as React from 'react'
import { getQuestionHistory } from '../../../lib'
import Bar from '../../Icon/Bar'
import { useDispatch } from 'react-redux'
import { show, destroy } from 'redux-modal'
import { MODAL } from '../../Modal'
import { IDBResult } from '../../../interfaces'
import QuestionHistory from './QuestionHistory'

const History: React.FC<{ question_id: string }> = ({ question_id }) => {
  const dispatch = useDispatch()
  const [result, setResult] = React.useState<IDBResult>()

  React.useEffect(() => {
    getQuestionHistory(question_id)
      .then((historyResult: IDBResult) =>
        setResult(historyResult)
      )
  }, [question_id])

  if (!result) {
    return null
  }

  const hasIncorrectResults = result.results.filter(r => !r.correct).length > 0

  if (!hasIncorrectResults) {
    return null
  }

  return (
    <React.Fragment>
      <span
        onClick={() => dispatch(show(MODAL, {
          content: <QuestionHistory result={result}
            destroy={() => dispatch(destroy(MODAL))}
          />
        }))}><Bar />
      </span>
    </React.Fragment>
  )
}

export default History