import * as React from 'react'
import { useState } from '../../hooks'
import Intro from '../Base/Quiz/Intro'
import Options from '../Base/Quiz/Options'
import Quiz from './Quiz'
import Results from '../Base/Quiz/Results'

const VerbTeFormTester: React.FC<{}> = () => {
  const activity = useState(s => s.activity)

  return activity.running
    ? <Quiz activity={activity} />
    : activity.complete
      ? <Results activity={activity} />
      : (
        <React.Fragment>
          <Intro activity={activity} />
          <Options activity={activity} />
        </React.Fragment>
      )
}

export default VerbTeFormTester
