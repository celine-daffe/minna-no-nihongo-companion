import * as React from 'react'
import styled from '@emotion/styled'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../Panel'
import Button from '../Button'
import { useDispatch } from 'react-redux'
import { quitActivity, submitAnswer, skipQuestion } from '../../store/actions/activity'
import { IQuestion, IGrammarItem } from '../../interfaces'
import { theme } from '../../data'
import Question from './Question'
import { IActivityState } from '../../store/reducers/activity'

const OptionsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 0.7rem;

  button + button {
    margin-left: 0;
  }
`

const Quiz: React.FC<{
  activity: IActivityState
}> = ({ activity }) => {
  const dispatch = useDispatch()

  const { correct, currentQuestion } = activity
  const { question, options, questionNumber } = currentQuestion as IQuestion

  const { green, red } = theme.colours

  return (
    <Panel borderTop={typeof correct === "boolean" ? correct ? green.dark : red.dark : 'transparent'}>
      <PanelHeading heading={`Q.${questionNumber}`} />
      <PanelDivider />
      <Question ruleItem={question as IGrammarItem} />
      <PanelDivider />
      <PanelActions>
        <OptionsContainer>
          {options.map((option: string, index: number) =>
            <Button
              key={`answer-${index}`}
              label={option}
              action={() => dispatch(submitAnswer([option]))}
            />
          )}
        </OptionsContainer>
      </PanelActions>
      <PanelDivider />
      <PanelActions>
        <Button label='Quit' action={() => dispatch(quitActivity())} />
        <Button label='Skip Question' action={() => dispatch(skipQuestion())} />
      </PanelActions>
    </Panel>
  )
}

export default Quiz
