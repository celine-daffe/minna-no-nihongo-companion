import * as React from 'react'
import { IGrammarItem } from '../../interfaces'
import Conversation from './Question/Conversation'
import Sentence from './Question/Sentence'
import styled from '@emotion/styled'

const QuestionContainer = styled.div`
  text-align: center;

  blockquote {
    line-height: 1.1em;
  }
`

const Question: React.FC<{ ruleItem: IGrammarItem }> = (props) => {
  return (
    <QuestionContainer>
      {
        typeof props.ruleItem.hiragana === "string"
          ? <Sentence {...props} />
          : <Conversation {...props} />
      }
    </QuestionContainer>
  )
}

export default Question
