import * as React from 'react'
import { withRuby } from '../../../lib'
import { IGrammarItem } from '../../../interfaces'
import styled from '@emotion/styled'

const SentenceContainer = styled.blockquote`
  line-height: 2em;

  span span {
    display: inline-block;
    background-color: #eee;
    min-width: 1.6rem;
    border: 1px solid #ddd;
  }
`

const Sentence: React.FC<{ ruleItem: IGrammarItem }> = ({ ruleItem }) =>
  <SentenceContainer>
    <span dangerouslySetInnerHTML={{ __html: withRuby(ruleItem.hiragana as string) }} />
  </SentenceContainer>

export default Sentence