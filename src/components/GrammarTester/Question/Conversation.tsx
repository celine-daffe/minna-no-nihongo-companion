import * as React from 'react'
import styled from '@emotion/styled'
import { withRuby } from '../../../lib'
import { IConversationItem, IGrammarItem } from '../../../interfaces'

const ConversationLine = styled.blockquote`
  svg {
    position: relative;
    top: 0.1em;
    right: 0.5rem;
  }

  + blockquote {
    margin-top: 1rem;
  }

  span span {
    display: inline-block;
    background-color: #eee;
    min-width: 2rem;
  }
`

const Conversation: React.FC<{ ruleItem: IGrammarItem }> = ({ ruleItem }) => (
  <React.Fragment>
    {
      (ruleItem.hiragana as IConversationItem[])
        .map((item: IConversationItem, index: number) =>
          <ConversationLine key={`line-${index}`}>
            <span dangerouslySetInnerHTML={{ __html: withRuby(item.line) }} />
          </ConversationLine>
        )
    }
  </React.Fragment>
)

export default Conversation