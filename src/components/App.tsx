import * as React from 'react'
import styled from '@emotion/styled'
import Navbar from './Navbar'
import ActivityPanel from './ActivityPanel'
import Activity from './Activity'
import { activities, theme } from '../data'
import { useDispatch } from 'react-redux'
import { useState } from '../hooks'
import { setActivity } from '../store/actions/activity'
import { ActivityType } from '../interfaces'
import Modal from './Modal'

const AppContainer = styled.main`
  font-family: ${theme.fontFamily};
  background-color: ${theme.colours.grey.light};
  margin: 1rem;
  max-width: ${theme.containers.main.maxWidth};
  width: 95%;
`

// @TODO this conditional is a bit shit eh?

const App: React.FC<{}> = () => {
  const state = useState(s => s)

  if (state.activity.currentActivity.type !== ActivityType.NONE) {
    return (
      <React.Fragment>
        <Navbar />
        <AppContainer>
          <Activity activity={state.activity.currentActivity} />
        </AppContainer>
        <Modal />
      </React.Fragment>
    )
  }

  const dispatch = useDispatch()

  return (
    <React.Fragment>
      <Navbar />
      <AppContainer>
        {activities.map(activity => (
          <ActivityPanel
            key={activity.type}
            heading={activity.title}
            subheading={activity.subtitle}
            actions={[
              {
                label: 'Play',
                action: () => dispatch(setActivity(activity))
              }
            ]}
          />
        ))}
      </AppContainer>
      <Modal />
    </React.Fragment>
  )
}

export default App