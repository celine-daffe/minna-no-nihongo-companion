import styled from '@emotion/styled'
import { theme } from '../data'

const Input = styled.input`
  width: calc(100% - 10px);
  margin: 0;
  padding: 5px 2px;
  font-family: ${theme.fontFamily};
  font-size: ${theme.fontSize.slightlySmaller};
  font-weight: lighter;
`

export default Input