import * as React from 'react'
import { useDispatch } from 'react-redux'
import Panel, { PanelHeading, PanelDivider, PanelActions } from '../Panel'
import Button from '../Button'
import VerbList from './List'
import VerbListFilter from './ListFilter'
import { useState } from '../../hooks'
import { setVerbs, resetVerbs } from '../../store/actions/list'
import { backToMenu } from '../../store/actions/activity'
import { filterCollectionByStringOrChapter } from '../../lib'

const VerbDictionary: React.FC<{}> = () => {
  const dispatch = useDispatch()
  const state = useState(s => s)
  const { list } = state

  const close = () => {
    dispatch(
      backToMenu()
    )
  }

  const filterVerbs = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = event.target

    if (value && value.length) {
      dispatch(
        setVerbs(
          filterCollectionByStringOrChapter(list.verbs, value)
        )
      )
    } else {
      dispatch(resetVerbs())
    }
  }

  return (
    <Panel>
      <PanelHeading
        heading='Verb list'
        subheading='Filter by english, hiragana, or chapter number'
      />
      <PanelDivider />
      <PanelActions>
        <Button label="Back to Menu" action={() => close()}/>
      </PanelActions>
      <PanelDivider />
      <VerbListFilter onChange={filterVerbs} />
      <PanelDivider />
      <VerbList verbs={list.filteredVerbs} />
    </Panel>
  )
}

export default VerbDictionary
