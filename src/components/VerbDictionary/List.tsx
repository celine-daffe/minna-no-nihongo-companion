import * as React from 'react'
import styled from '@emotion/styled'
import { Verb } from 'minna-no-nihongo-verbs'
import VerbItem from '../VerbItem'
import { MODAL } from '../Modal'
import { useDispatch } from 'react-redux'
import { show, destroy } from 'redux-modal'
import BaseTable from '../Base/Table'
import { theme } from '../../data'

const Table = styled(BaseTable)`
  th {
    padding: 0.5rem;
    background-color: ${theme.colours.grey.dark}
  }

  td:nth-of-type(2) {
    width: 1rem;
    text-align: center;
  }
`

const VerbList: React.FC<{ verbs: Verb[] }> = ({ verbs }) => {
  const dispatch = useDispatch()

  const showModal = (v: Verb) => {
    dispatch(show(MODAL, {
      content: <VerbItem
        verb={v}
        destroy={() => dispatch(destroy(MODAL))}
      />
    }))
  }

  return (
    <Table>
      <thead>
        <tr>
          <th>Hiragana</th>
          <th>Chapter</th>
          <th>English</th>
        </tr>
      </thead>
      <tbody>
        {verbs.map((v) => (
          <tr key={v.id} onClick={() => showModal(v)}>
            <td>{v.polite_form.present} ({v.verb_group})</td>
            <td>{v.chapter}</td>
            <td>{v.english}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  )
}

export default VerbList
