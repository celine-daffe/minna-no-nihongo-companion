import * as React from 'react'
import Input from '../Input'

const VerbListFilter: React.FC<{
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}> = ({ onChange }) => (
  <Input type="text" placeholder="Filter..." onChange={onChange} />
)

export default VerbListFilter
