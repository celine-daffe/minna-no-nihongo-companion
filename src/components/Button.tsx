import * as React from 'react'
import styled from '@emotion/styled'
import { theme } from '../data'

interface IButtonStyleProps {
  variant?: ButtonVariant
  correct?: null | boolean
}

const StyledButton = styled.button<IButtonStyleProps>`
  padding: 0.5rem;
  font-size: 0.8rem;
  letter-spacing: 1px;
  text-transform: uppercase;
  touch-action: manipulation;
  outline-color: #444;
  line-height: 1.42857143;
  border-radius: 2px;
  border: 2px solid #e6e6e6;
  font-weight: 500;
  background: ${props => props.variant
    ? theme.buttons.variants[props.variant].backgroundColor
    : '#efefef'
  };
`

type ButtonVariant = 'red' | 'orange' | 'black'

interface IButtonProps {
  label: string
  action: () => any
  icon?: string
  className?: string
  variant?: ButtonVariant
  correct?: null | boolean
}

const Button: React.FC<IButtonProps> = (props) => {
  const { action = () => null, className, label, variant, correct = null } = props
  const answered = typeof correct === "boolean"

  return (
    <StyledButton
      className={className}
      onClick={action}
      variant={variant}
      disabled={answered}
      correct={answered && correct}
    >
      {label}
    </StyledButton>
  )
}

export default Button