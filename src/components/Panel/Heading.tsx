import * as React from 'react'
import styled from '@emotion/styled'
import { theme } from '../../data'

interface IPanelHeadingProps {
  heading?: string
  subheading?: string | string[]
}

const Heading = styled.h2`
  margin: 0;
  text-align: left;
  margin-bottom: 0.3rem;
  font-size: 1.1em;
`

const Subheading = styled.h4`
  margin: 0;
  text-align: left;
  font-weight: lighter;
  font-size: ${theme.fontSize.slightlySmaller};

  + h4 {
    padding-top: 0.2rem;
  }
`

export const PanelHeading: React.FC<IPanelHeadingProps> = ({ heading, subheading }) => {
  const subheadings = Array.isArray(subheading) ? subheading : [subheading]

  return (
    <React.Fragment>
      {
        heading
          ? <Heading
              dangerouslySetInnerHTML={{ __html: heading }}
            />
          : null
      }
      {
        subheading
          ? subheadings.map((s) => <Subheading key={s}>{s}</Subheading>)
          : null
      }
    </React.Fragment>
  )
}
