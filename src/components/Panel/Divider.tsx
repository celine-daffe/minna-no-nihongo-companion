import styled from '@emotion/styled'
import { theme } from '../../data'

export const PanelDivider = styled.hr`
  border-top: 1px solid ${theme.colours.grey.medium};
  color: transparent; 
  margin: 0.7rem 0;
`