import * as React from 'react'
import styled from '@emotion/styled'
import Text from './Text'

interface ITextProps {
  highlight?: boolean
}

const Highlight = styled.span<ITextProps>`
  color: ${props => props.highlight ? 'red' : 'inherit'}
`

const HighlightedText: React.FC<{
  text: string,
  highlight: string
}> = ({ text, highlight }) => {
  const pieces = text.split(highlight)

  if (pieces.length) {
    return (
      <Text>
        {
          pieces.map((p: string, index: number) => (
            <React.Fragment key={index}>
              {p}
              {index !== pieces.length -1 ? <Highlight highlight>{highlight}</Highlight> : null}
            </React.Fragment>
          ))
        }
      </Text>
    )
  }

  return <Text>{text}</Text>
}

export default HighlightedText
