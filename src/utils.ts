import { IChapterRangeOptions } from "./interfaces"

const { random, floor } = Math

export const randomArrayItem = <T>(items: T[]): T | undefined =>
  items.length
    ? items.length === 1
      ? items[0]
      : items[floor(random() * items.length)]
    : undefined

export const randomArrayItems = (items: any[], numberOfItems: number): any =>
  Array.apply(null, { length: numberOfItems }).map(() => randomArrayItem(items))

export const equalArrays = (leftItems: any[], rightItems: any[]): boolean =>
  leftItems.length === rightItems.length &&
  leftItems.sort().every((value, index) => value === rightItems.sort()[index])

export const padArrayWith = (items: any[], padding: any[], desiredLength: number) => {
  if (items.length >= desiredLength) {
    return items.slice(0, desiredLength)
  }

  return shuffle(items.concat(padding
    .filter(i => items.indexOf(i) === -1)
    .slice(0, desiredLength - items.length))
  )
}

export const shuffle = (items: any[]): any[] =>
  items.map((i) => [random(), i]).sort().map(t => t[1])

export const numberedArrayItems = <T>(items: T[]): T[] =>
  items.map((item, index: number) => ({
    ...item,
    questionNumber: index + 1
  }))

export const stripWhitespace = (
  input: string
): string => input.split(' ').filter(i => i.length).join('')

export const stripPunctuation = (input: string): string => 
  input.replace(/[.,~()。、？、?]/g, '')

const isPlainObject = (i: any): boolean => !!i && typeof i === 'object' && !Array.isArray(i)

export const mergeObjects = <T extends any>(...objs: T[]): T => {
  const ret: any = {}

  for (let i: number = 0, l: number = objs.length; i < l; i++) {
    const currentObj: any = objs[i]

    if (isPlainObject(currentObj)) {
      const keys: string[] = Object.keys(currentObj)

      for (let j: number = 0, k: number = keys.length; j < k; j++) {
        const key: string = keys[j]
        const member: any = currentObj[key]

        if (isPlainObject(member)) {
          ret[key] = isPlainObject(ret[key] || null)
            ? mergeObjects(ret[key], member)
            : mergeObjects(member)
        } else if (Array.isArray(member)) {
          ret[key] = Array.isArray(ret[key] || null)
            ? ret[key].concat(member)
            : member
        } else {
          ret[key] = currentObj[key]
        }
      }
    }
  }

  return ret
}

export const isNumber = (v: any): boolean =>
  (!isPlainObject(v) && !Array.isArray(v) && (v != null) && !isNaN(Number(v.toString())))

export const filterByChapterRange = <T extends { ruleId: number }>(
  collection: T[],
  options: IChapterRangeOptions
): T[] => {
  const { MIN_CHAPTER = 1, MAX_CHAPTER = 25 } = options

  return collection.filter((i) => i.ruleId >= MIN_CHAPTER && i.ruleId <= MAX_CHAPTER)
}
