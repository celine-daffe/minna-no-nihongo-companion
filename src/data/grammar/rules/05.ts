import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.HE_DESTINATION,
    particles: [
      'へ'
    ],
    rule: '[N.Place] へ 行きます / きます / かえります',
    explanation:
      `When a verb indicates movement to a certain place, the particle 'へ' is added after the
       place noun to show the direction of movement. This is true when no activity takes places at the location,
       in such cases 'で' is used instead.`
  },
  {
    id: Keys.MO_DENY_INTERROGATIVE,
    particles: [
      'も'
    ],
    rule: '...も いきません / たべません / しません',
    explanation:
      `When you want to deny everything covered by an question, attach も and put the verb in its negative form`
  },
  {
    id: Keys.DE_TRANSPORT_METHOD,
    particles: [
      'で'
    ],
    rule: '[N.Method] で 行きます / きます / かえります',
    explanation:
      `The particle で in this form indicates a means or method of travel. For example [Train, Bus, Car] で [Location]`
  },
  {
    id: Keys.TO_ACTION_TOGETHER,
    particles: [
      'と'
    ],
    rule: '[N.LivingThing] と [V]',
    explanation:
      `When talking about performing an action with a person or animal, the person or animal is denoted by と`
  },
] as IGrammarRule[]
