import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.I_THINK_POSITIVE_CONJECTURE,
    particles: [
      'と'
    ],
    rule: 'Incomplete',
    explanation: 'Incomplete'
  },
  {
    id: Keys.I_THINK_NEGATIVE_CONJECTURE,
    particles: [
      'と'
    ],
    rule: 'Incomplete',
    explanation: 'Incomplete'
  },
] as IGrammarRule[]
