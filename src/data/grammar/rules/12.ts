import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.YORI_COMPARISON,
    particles: [
      'より'
    ],
    rule: '[N.1] は [N.2] より [Adjective] です',
    explanation:
      `Describes the character and/or state of N.1 compared to to N.2. N.2 is the winner, so saying 
      'この くるま は その くるま より たかい です' you are stating that the second car is more expensive.`
  },
  {
    id: Keys.DE_CATEGORY,
    particles: [
      'で'
    ],
    rule: '[Group] で いちばん [Property] です か',
    explanation:
      `When asking about a group or category (food, weather, ...etc) the group definition is followed by で`
  },
] as IGrammarRule[]
