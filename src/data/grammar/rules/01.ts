import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.MO_ALSO,
    particles: [
      'も'
    ],
    rule: '[N.1] も...',
    explanation:
      `も roughly indicates 'also', and replaces は or が when referring to the subject. '私も' is a basic
       way of declaring 'Me too'`
  },
  {
    id: Keys.NO_OWNERSHIP,
    particles: [
      'の'
    ],
    rule: '[N.1] の [N.2]',
    explanation:
      `In this sentence form, の indicates ownership or belonging.`
  }
] as IGrammarRule[]
