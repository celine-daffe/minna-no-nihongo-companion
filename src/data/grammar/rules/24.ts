import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.GIVING_RECEIVING_AGEMASHITA,
    particles: [
      'あげます',
    ],
    decoys: [
      'くれました',
      'もらいました',
      'あげる',
      'もらった'
    ],
    rule: '私 は [Person] に [N] を あげました',
    explanation:
      `The general form of describing yourself giving something to someone else. In this form, 
       you are the subject, the person receicing is the topic, and the verb is what you did (giving)`
  },
  {
    id: Keys.GIVING_RECEIVING_MORAIMASHITA,
    particles: [
      'もらいます',
    ],
    decoys: [
      'くれました',
      'あげました',
      'あげる',
      'もらった'
    ],
    rule: '[Person.1] は [Person.2] に [N] を もらいます',
    explanation:
      `The general form of describing someone receiving something from someone else. Either can be わたし. 
       もらいます does not need to be handled differently according to givers and receivers`
  },
  {
    id: Keys.GIVING_RECEIVING_KUREMASHITA,
    particles: [
      'くれます',
    ],
    rule: '[Person] は わたし に [N] を くれます',
    explanation:
      `The general form of describing someone giving something to you, or a member of your family. In this case,
       あげます is replaced by くれます.`
  },
  {
    id: Keys.GIVING_RECEIVING_TE_AGEMASHITA,
    particles: [
      'くれます'
    ],
    rule: '[Person] は 私に [V.TeForm] を あげました',
    explanation:
      `As well as communicating a physical object received from a person with くれました, it can be also be used
      to describe an action which you were a beneficiary. If someone makes something for you, buys something for you
      or lends something to you, くれました is used with the Verb in て-form. The giver is denoted by に. If the receiver is
      私, it is often left out`
  }
] as IGrammarRule[]
