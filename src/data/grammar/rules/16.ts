import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    particles: [
      'して'
    ],
    rule: '[..Verbs.TeForm] [V] [Tense]',
    explanation:
      `When expressing several actions, the て-forms of the actions can be listed 
      until the final action, which uses the polite tense form to indicate tense of all actions`
  },
  {
    id: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    particles: [
      'くて'
    ],
    rule: '[いい-Adj - い] くて...',
    explanation:
      `When listing several い adjectives, the い is dropped and くて is appended. 
      For example, ちさい becomes ちさくて. If two ajectives are not aligned
      (for exaple, positive + negative) が should be used in place of くて`
  },
  {
    id: Keys.TE_FORM_NA_ADJ_JOIN_SENTENCES,
    particles: [
      'で'
    ],
    rule: '[な-Adj - な] で...',
    explanation:
      `When listing several な adjectives, the な is dropped and で is appended.
      For example, とうきょう は きれい (-な) で, しずか まち です. If two ajectives are not aligned
      (for exaple, positive + negative) が should be used in place of で`
  },
  {
    id: Keys.TE_FORM_NOUN_DE_JOIN_SENTENCES,
    particles: [
      'で'
    ],
    rule: '[N] で...',
    explanation:
      `When listing items which end in nouns, such as declaring some type of object or state, the noun
      is followed by で. For example, I am a student, my friend is a doctor:
      わたし は がくえい で, ともだち は いしや です`
  },
  {
    id: Keys.TE_FORM_KARA_THEN,
    particles: [
      'から'
    ],
    rule: '[V1.TeForm から V2]',
    explanation:
      `This structure enables ordering actions, and indicates that the action following から
       depends upon the first action being completed.`
  },
  {
    id: Keys.NOUN_GA_ADJECTIVE,
    particles: [
      'が'
    ],
    rule: '[N.1] を [N.2] が [Adj]',
    explanation:
      `This structure allows describing a topic [N.1] as having characteristic [N.2 が Adj]. This can describe
      a place (N1: おきなわ) having beautiful oceans (うめ が きれあな).`
  },
  {
    id: Keys.NOUN_WO_VERB,
    particles: [
      'を'
    ],
    rule: '[N を V]',
    explanation:
      `When a verb communicates motion with a starting point, を is used to specify the starting point. For example
      Leaving (でます) from work would be しごと を でます.`
  },
  {
    id: Keys.DOUYATTE,
    particles: [
      'どうやって'
    ],
    rule: '[...condition] どうやって [...process] か?',
    explanation:
      `どうやって is used as a question as to how a person does something. For example, how a person 
      travels to a destination.`
  },
  {
    id: Keys.DORE_DONO,
    particles: [
      'どれ',
      'どの'
    ],
    rule: 'どれ / どの [N]',
    explanation:
      `TODO`
  },
] as IGrammarRule[]
