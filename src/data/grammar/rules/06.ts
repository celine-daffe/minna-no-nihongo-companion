import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.DE_ACTION_AT_DESTINATION,
    particles: [
      'で'
    ],
    rule: '[Place] で [Verb] います',
    explanation:
      `When added after a noun denoting a place, で indicates an action taking place at that location`
  },
] as IGrammarRule[]
