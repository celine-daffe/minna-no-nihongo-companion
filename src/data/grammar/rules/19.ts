import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    particles: [
      'こと'
    ],
    rule: '[V.TaForm] こと が あります',
    explanation:
      `This sentence pattern is used to say what happened in the past as a particular experience`
  },
  {
    id: Keys.TA_FORM_LIST,
    particles: [
      'り'
    ],
    rule: '[V1.TaForm]り、[V2.TaForm]り、[VN...]り します',
    explanation:
      `When listing verbs, や is replaced with り attached to the verb.
       Tense is declared at the end of the sentence.`
  },
] as IGrammarRule[]
