import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.NI_TIME,
    particles: [
      'に'
    ],
    rule: '[N] (time) に [V]',
    explanation:
      `The particle に is appended to a noun and indicates the time of an occurance. This is often ommitted in later
       examples.`
  },
  {
    id: Keys.MADE_KARA_BEGIN_END,
    particles: [
      'から',
      'まで'
    ],
    rule: '[N.1] から [N.2] まで',
    explanation:
      `'から' indicates a starting time or place, while 'まで' indicates a finishing time or place`,
  },
  {
    id: Keys.TO_COORDINATE_RELATION,
    particles: [
      'と'
    ],
    rule: '[N.1] と [N.2]',
    explanation:
      `The と particle connects two related nouns. For example eggs and bread can be 'たまご と パン'`
  },
] as IGrammarRule[]
