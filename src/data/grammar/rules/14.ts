import { Keys } from '../keys'
import { IGrammarRule } from '../../../interfaces'

export default [
  {
    id: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    particles: [
      'が'
    ],
    rule: 'すみませんが ... [V.TeForm] ください',
    explanation:
      `This pattern is used to request listener to do something. Prepending すみませんが makes it polite`
  },
  {
    id: Keys.TE_FORM_PLEASE_DO_TELLING,
    particles: [
      'ください'
    ],
    rule: '[Instruction]...[V.TeForm] ください',
    explanation:
      `This pattern is more firm, and is used to tell someone to do something specific`
  },
  {
    id: Keys.TE_FORM_PLEASE_DO_INVITING,
    particles: [
      'ください'
    ],
    rule: '[Invitation]...[V.TeForm] ください',
    explanation:
      `This pattern is more polite, and is used to invite someone to do something specific`
  },
  {
    id: Keys.TE_FORM_IN_PROGRESS,
    particles: [
      'います'
    ],
    rule: '[V.TeForm] います',
    explanation: 'This pattern indicates that a certain action or motion is in progress'
  },
  {
    id: Keys.TE_FORM_SHALL_I,
    particles: [
      'を'
    ],
    rule: '...[V.Volitional] か',
    explanation:
      `This form is used to offer an action in the form of a verb, such as carry, help...`
  },
] as IGrammarRule[]
