import { IGrammarItem } from '../../interfaces'

import { default as chapter01 } from './items/01'
import { default as chapter02 } from './items/02'
import { default as chapter03 } from './items/03'
import { default as chapter04 } from './items/04'
import { default as chapter05 } from './items/05'
import { default as chapter06 } from './items/06'
import { default as chapter10 } from './items/10'
import { default as chapter12 } from './items/12'
import { default as chapter13 } from './items/13'
import { default as chapter14 } from './items/14'
import { default as chapter17 } from './items/17'
import { default as chapter18 } from './items/18'
import { default as chapter22 } from './items/22'
import { default as chapter23 } from './items/23'
import { default as chapter24 } from './items/24'

export const grammarItems: IGrammarItem[] = [
  ...chapter01,
  ...chapter02,
  ...chapter03,
  ...chapter04,
  ...chapter05,
  ...chapter06,
  ...chapter10,
  ...chapter12,
  ...chapter13,
  ...chapter14,
  ...chapter17,
  ...chapter18,
  ...chapter22,
  ...chapter23,
  ...chapter24,
]
