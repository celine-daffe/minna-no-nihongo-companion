import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: '-MXusJdtK',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみません {が} この かんじ の よみかた を おしえて ください',
    english: 'Excuse me, could you tell me how to read this kanji please?'
  },
  {
    id: 'Jvn_ISfw2',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'なまえ を かいて {ください}',
    english: 'Please write your name'
  },
  {
    id: 'dzPwATYT3',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'どうぞ たくさん たべて {ください}',
    english: 'Please eat as much as you want'
  },
  {
    id: '91GiCQPFs',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'いま あめ が ふって {います} か',
    english: 'Is it raining right now?'
  },
  {
    id: 'BK1sPG_uQ',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: [
      {
        line: 'いま あめ が ふって {います} か'
      },
      {
        line: 'はい ふって います'
      }
    ]
  },
  {
    id: 'lWs5gqiqn',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'にもつ {を} もちましょうか',
    english: 'Can I carry your luggage?'
  },
] as IGrammarItem[]