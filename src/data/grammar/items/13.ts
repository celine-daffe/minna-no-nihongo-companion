import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'tLASBFKzC',
    ruleId: Keys.HOSHII,
    hiragana: '私は ともだち {が} {ほしい} です',
    answer: [
      'か',
      'ほしい'
    ],
    english: 'I want some friends'
  },
] as IGrammarItem[]