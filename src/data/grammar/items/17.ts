import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'EDNZUFgMm',
    ruleId: Keys.NAI_FORM_POLITE_REQUEST,
    hiragana: 'ここで すわ {ない} {で} ください',
    english: 'Please don\'t sit here'
  },
] as IGrammarItem[]