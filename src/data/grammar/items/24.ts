import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'WXpG0YVbo',
    ruleId: Keys.GIVING_RECEIVING_AGEMASHITA,
    hiragana: 'わたし は せんせい に はな を {あげました}',
    english: 'I gave flowers to my teacher'
  },
  {
    id: 'wke2a3tk2',
    ruleId: Keys.GIVING_RECEIVING_TE_AGEMASHITA,
    hiragana: 'ちち は おかね を かして {くれました}',
    english: 'Dad lent me some money'
  },
] as IGrammarItem[]