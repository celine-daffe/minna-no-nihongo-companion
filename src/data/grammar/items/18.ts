import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'A1WhijIFY',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: 'わたし は にほんご {が} できます',
    english: 'I can speak Japanese'
  },
  {
    id: 'vQSqL9FCW',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: 'わたし は ピアノ {が} できません',
    english: 'I can\'t play the piano'
  },
  {
    id: 'BzSFVqEOA',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: [
      {
        source: 'A',
        line: 'ピアノ {が} できます か'
      },
      {
        source: 'B',
        line: 'いいえ できません'
      }
    ],
  },
  {
    id: 'IOuX6bFi0',
    ruleId: Keys.DEKIMASU_VERB,
    hiragana: '私は かんじ を よむ {こと} {が} できます',
    english: 'I can read Kanji'
  },
  {
    id: 'wkQRVn20P',
    ruleId: Keys.DEKIMASU_VERB,
    hiragana: 'かード で はらう {こと} {が} できます',
    english: 'You can pay by card'
  },
  {
    id: 'UCqYkS9FA',
    ruleId: Keys.HOBBIES_NOUN_FORM,
    hiragana: 'わたし の しゅみ {は} おんがく です'
  },
  {
    id: 'MxY85pPvs',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は おんがく {を} きく {こと} です',
    english: 'My hobby is listening to music',
  },
] as IGrammarItem[]