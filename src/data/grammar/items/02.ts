import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: 'd8XMmIHwfg',
    ruleId: Keys.NO_SUBJECT,
    hiragana: [
      {
        line: 'それは だれ の ほんですか',
      },
      {
        line: 'わたし {の} ほんです',
      }
    ]
  },
  {
    id: '04UdCbKxf',
    ruleId: Keys.NO_SUBJECT,
    hiragana: [
      {
        line: 'それは なに の ほん ですか',
      },
      {
        line: 'コーヒー {の} ほん です',
      }
    ]
  }
] as IGrammarItem[]