import { Keys } from '../keys'
import { IGrammarItem } from '../../../interfaces'

export default [
  {
    id: '-nIJD8YY9-j',
    ruleId: Keys.DE_ACTION_AT_DESTINATION,
    hiragana: 'わたし は こうえん {で} テニス を します',
    english: 'I played tennis in the park'
  },
  {
    id: '605DcW_Piip',
    ruleId: Keys.DE_ACTION_AT_DESTINATION,
    hiragana: 'えき {で} しんぶん を かいます',
    english: 'I bought a newspaper from the train station'
  },
] as IGrammarItem[]