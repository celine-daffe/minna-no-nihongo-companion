import {
  IActivityOption,
  OptionKey,
  OptionType,
  VerbType,
  VerbTypeLabels
} from '../interfaces/'

export const verbTypeOptions = [
  {
    label: VerbTypeLabels[VerbType.POLITE],
    value: VerbType.POLITE
  },
  {
    label: VerbTypeLabels[VerbType.PLAIN],
    value: VerbType.PLAIN
  },
  {
    label: VerbTypeLabels[VerbType.NAI],
    value: VerbType.NAI
  },
  {
    label: VerbTypeLabels[VerbType.TE],
    value: VerbType.TE
  },
  {
    label: VerbTypeLabels[VerbType.TA],
    value: VerbType.TA
  },
]

export const questionVerbForm: IActivityOption = {
  id: OptionKey.QUESTION_VERB_FORM,
  title: 'Question Form',
  type: OptionType.STRING,
  values: verbTypeOptions,
  default: VerbType.POLITE,
  dependency: {
    option: OptionKey.ANSWER_VERB_FORM,
    rule: 'distinct'
  }
}

export const answerVerbForm: IActivityOption = {
  id: OptionKey.ANSWER_VERB_FORM,
  title: 'Answer Form',
  type: OptionType.STRING,
  values: verbTypeOptions,
  default: VerbType.PLAIN,
  dependency: {
    option: OptionKey.QUESTION_VERB_FORM,
    rule: 'distinct'
  }
}

export const verbFormOptions: IActivityOption[] = [
  questionVerbForm,
  answerVerbForm
]

export const numberOfQuestionsOption: IActivityOption = {
  id: OptionKey.NUM_QUESTIONS,
  title: 'Number of Questions',
  type: OptionType.NUMBER,
  default: 10
}

export const chapterRangeOptions: IActivityOption[] = [
  {
    id: OptionKey.MIN_CHAPTER,
    title: 'Minimum Chapter',
    type: OptionType.NUMBER,
    default: 1,
    dependency: {
      option: OptionKey.MAX_CHAPTER,
      rule: 'max'
    }
  },
  {
    id: OptionKey.MAX_CHAPTER,
    title: 'Maximum Chapter',
    type: OptionType.NUMBER,
    default: 25,
    dependency: {
      option: OptionKey.MIN_CHAPTER,
      rule: 'min'
    }
  }
]
