import { IHiraganaMatch, IKanjiMatch } from '../interfaces'

export const kanji: IKanjiMatch[] = [
  {
    kanji: '父',
    ruby: [{
      kanji: '父',
      hiragana: 'ちち'
    }]
  },
  {
    kanji: '美味',
    ruby: [{
      kanji: '美',
      hiragana: 'お'
    },
    {
      kanji: '味',
      hiragana: 'い'
    }]
  },
  {
    kanji: '古',
    ruby: [{
      kanji: '古',
      hiragana: 'ふる'
    }]
  },
  {
    kanji: '長',
    ruby: [{
      kanji: '長',
      hiragana: 'なが'
    }]
  },
  {
    kanji: '白',
    ruby: [{
      kanji: '白',
      hiragana: 'しろ'
    }]
  },
  {
    kanji: '面白',
    ruby: [{
      kanji: '面',
      hiragana: 'おも'
    }, {
      kanji: '白',
      hiragana: 'しろ'
    }]
  },
  {
    kanji: '弱',
    ruby: [{
      kanji: '弱',
      hiragana: 'よわ'
    }]
  },
  {
    kanji: '難',
    ruby: [{
      kanji: '難',
      hiragana: 'むずか'
    }]
  },
  {
    kanji: '優',
    ruby: [{
      kanji: '優',
      hiragana: 'やさ'
    }]
  },
  {
    kanji: '冷',
    ruby: [{
      kanji: '冷',
      hiragana: 'さむ'
    }]
  },
  {
    kanji: '寒',
    ruby: [{
      kanji: '寒',
      hiragana: 'さむ'
    }]
  },
  {
    kanji: '暑',
    ruby: [{
      kanji: '暑',
      hiragana: 'あつ'
    }]
  },
  {
    kanji: '悪',
    ruby: [{
      kanji: '悪',
      hiragana: 'わる'
    }]
  },
  {
    kanji: '新',
    ruby: [{
      kanji: '新',
      hiragana: 'あたら'
    }]
  },
  {
    kanji: '大',
    ruby: [{
      kanji: '大',
      hiragana: 'おお'
    }]
  },
  {
    kanji: '賑',
    ruby: [{
      kanji: '賑',
      hiragana: 'にぎ'
    }]
  },
  {
    kanji: '静',
    ruby: [{
      kanji: '静',
      hiragana: 'しず'
    }]
  },
  {
    kanji: '静か',
    ruby: [{
      kanji: '静',
      hiragana: 'しず'
    }]
  },
  {
    kanji: '遅い',
    ruby: [{
      kanji: '遅',
      hiragana: 'おそ'
    }]
  },
  {
    kanji: '重',
    ruby: [{
      kanji: '重',
      hiragana: 'おむ'
    }]
  },
  {
    kanji: '大きい',
    ruby: [{
      kanji: '大',
      hiragana: 'おお'
    }]
  },
  {
    kanji: '大変',
    ruby: [{
      kanji: '大',
      hiragana: 'たい'
    },
    {
      kanji: '変',
      hiragana: 'へん'
    }]
  },
  {
    kanji: '帰',
    ruby: [{
      kanji: '帰',
      hiragana: 'かえ'
    }]
  },
  {
    kanji: '不便',
    ruby: [{
      kanji: '不',
      hiragana: 'ふ'
    },
    {
      kanji: '便',
      hiragana: 'ぶん'
    }]
  },
  {
    kanji: '勉強',
    ruby: [{
      kanji: '勉',
      hiragana: 'べん'
    },
    {
      kanji: '強',
      hiragana: 'きょう'
    }]
  },
  {
    kanji: '大切',
    ruby: [{
      kanji: '大',
      hiragana: 'たい'
    },
    {
      kanji: '切',
      hiragana: 'せつ'
    }]
  },
  {
    kanji: '素敵',
    ruby: [{
      kanji: '素敵',
      hiragana: 'すてき',
    }]
  },
  {
    kanji: '無駄',
    ruby: [{
      kanji: '無',
      hiragana: 'む',
    },
    {
      kanji: '駄',
      hiragana: 'だ',
    }]
  },
  {
    kanji: '便利',
    ruby: [{
      kanji: '便',
      hiragana: 'べん',
    },
    {
      kanji: '利',
      hiragana: 'り',
    }]
  },
  {
    kanji: '簡単',
    ruby: [{
      kanji: '簡',
      hiragana: 'かん',
    },
    {
      kanji: '単',
      hiragana: 'たん',
    }]
  },
  {
    kanji: '有名',
    ruby: [{
      kanji: '有',
      hiragana: 'ゆう',
    },
    {
      kanji: '名',
      hiragana: 'めい',
    }]
  },
  {
    kanji: '暇',
    ruby: [{
      kanji: '暇',
      hiragana: 'ひま',
    }]
  },
  {
    kanji: '元気',
    ruby: [{
      kanji: '元',
      hiragana: 'げん',
    },
    {
      kanji: '気',
      hiragana: 'き',
    }]
  },
  {
    kanji: '親切',
    ruby: [{
      kanji: '親',
      hiragana: 'しん',
    },
    {
      kanji: '切',
      hiragana: 'せつ',
    }]
  },
  {
    kanji: '綺麗',
    ruby: [{
      kanji: '綺',
      hiragana: 'き',
    },
    {
      kanji: '麗',
      hiragana: 'れい',
    }]
  }
]

export const hiragana: IHiraganaMatch[] = [
  {
    hiragana: 'にほん',
    replace: 'にほん',
    kanji: '日本',
    add: ''
  },
  {
    hiragana: 'せんせい',
    replace: 'せんせい',
    kanji: '先生',
    add: ''
  },
  {
    hiragana: 'わたし',
    replace: 'わたし',
    kanji: '私',
    add: ''
  },
  {
    hiragana: 'いきます',
    replace: 'い',
    kanji: '行',
    add: 'きます'
  },
  {
    hiragana: 'いきました',
    replace: 'い',
    kanji: '行',
    add: 'きました'
  },
  {
    hiragana: 'いく',
    replace: 'い',
    kanji: '行',
    add: 'く '
  },
  {
    hiragana: 'ほん',
    replace: 'ほん',
    kanji: '本',
    add: ''
  },
  {
    hiragana: 'きます',
    replace: 'き',
    kanji: '来',
    add: 'ます'
  },
  {
    hiragana: 'きました',
    replace: 'き',
    kanji: '来',
    add: 'ました'
  },
  {
    hiragana: 'やすみ',
    replace: 'やす',
    kanji: '休',
    add: 'み'
  },
  {
    hiragana: 'あした',
    replace: 'あした',
    kanji: '明日',
    add: ''
  },
  {
    hiragana: 'きのう',
    replace: 'きのう',
    kanji: '昨日',
    add: ''
  },
  {
    hiragana: 'きょう',
    replace: 'きょう',
    kanji: '今日',
    add: ''
  },
]
