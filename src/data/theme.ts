interface IBasicColour {
  light: string
  dark: string
}

interface IDetailedColour extends IBasicColour {
  veryLight: string
  medium: string
  veryDark: string
}

interface ITheme {
  containers: {
    main: {
      maxWidth: string
    }
  }
  fontSize: {
    slightlySmaller: string
  },
  fontFamily: string
  colours: {
    white: string
    grey: IDetailedColour
    red: IBasicColour
    green: IBasicColour
  },
  buttons: Record<string, any>
}

export const theme: ITheme = {
  containers: {
    main: {
      maxWidth: '460px'
    }
  },
  fontSize: {
    slightlySmaller: '0.9rem'
  },
  fontFamily: 'Helvetica; sans-serif',
  buttons: {
    variants: {
      red: {
        backgroundColor: 'red'
      },
      orange: {
        backgroundColor: 'orange'
      },
      black: {
        backgroundColor: 'black'
      }
    }
  },
  colours: {
    white: '#fff',
    grey: {
      veryLight: '#e2e9e6',
      light: '#f5f5f5',
      medium: '#efefef',
      dark: '#cccccc',
      veryDark: '#333'
    },
    red: {
      light: '#f3d2d2;',
      dark: '#e66262'
    },
    green: {
      light: '#bdeabd',
      dark: '#78b178'
    }
  }
}