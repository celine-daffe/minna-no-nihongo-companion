import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

const teFormMayIFeedback = [
  {
    match: 'に',
    advice: 'Use the particle `で` in this structure, as the topic is a verb'
  }
]

const teFormLocationMayIFeedback = [
  {
    match: 'ここで',
    advice: 'Use the particle `に` when talking about the result of the verb (enter, climb, etc..)'
  }
]

const teFormMustNotDoFeedback = [
  {
    match: 'を',
    advice: 'Use `は` before いけません in this structure (the verb is the topic)'
  },
  {
    match: 'なければなりません',
    advice: 'Not used in this form.'
  }
]

const teFormOngoingStateFeedback = [
  {
    match: 'あります',
    advice: 'An ongoing state experienced by a person is declared using います'
  },
]

export const sentences: ISentence[] = [
  {
    id: 'n4yPoXCRi',
    ruleId: Keys.TE_FORM_MAY_I,
    english: `May I smoke here?`,
    hiragana: `ここ で すって も いいですか?`,
    feedback: [
      ...teFormMayIFeedback,
      {
        missing: 'すって',
        advice: 'The て-form of smoke is `すって`'
      }
    ]
  },
  {
    id: 'V54WWUuRi',
    ruleId: Keys.TE_FORM_MAY_I,
    english: `May I eat here?`,
    hiragana: `ここ で たべて も いいですか?`,
    feedback: [
      ...teFormMayIFeedback,
      {
        missing: 'たべて',
        advice: 'The て-form of eat is `たべて`'
      }
    ]
  },
  {
    id: 'TH29dd5UG',
    ruleId: Keys.TE_FORM_MAY_I,
    english: `May I sit here?`,
    hiragana: `ここ に すわて も いいですか?`,
    feedback: [
      ...teFormLocationMayIFeedback,
      {
        missing: 'すわて',
        advice: 'The て-form of sit is `すわて`'
      }
    ]
  },
  {
    id: 'RzxrZIHl4',
    ruleId: Keys.TE_FORM_MUST_NOT_DO,
    english: `You can't smoke here.`,
    hiragana: `ここ で すって は いけません`,
    feedback: [
      ...teFormMustNotDoFeedback,
      {
        missing: 'すって',
        advice: 'The て-form of sit is `すわて`'
      }
    ]
  },
  {
    id: 'RDLxhxJi5',
    ruleId: Keys.TE_FORM_MUST_NOT_DO,
    english: `You can't sleep here.`,
    hiragana: `ここ に ねて は いけません`,
    feedback: [
      ...teFormMustNotDoFeedback,
      {
        missing: 'ねて',
        advice: 'The て-form of sleep is `ねて`'
      }
    ]
  },
  {
    id: 'joQsyloJT',
    ruleId: Keys.TE_FORM_MUST_NOT_DO,
    english: `You musn't play here.`,
    hiragana: `ここ で あそんで は いけません`,
    feedback: [
      {
        missing: 'あそんで',
        advice: 'The て-form of play is `あそんで`'
      }
    ]
  },
  {
    id: 'quv4xiYd7',
    ruleId: Keys.TE_FORM_MUST_NOT_DO,
    english: `You can't come in here.`,
    hiragana: `ここ に はいて は いけません`,
    feedback: [
      ...teFormMustNotDoFeedback,
      {
        missing: 'はいて',
        advice: 'The て-form of enter is `はいて`'
      }
    ]
  },
  {
    id: 'joDueBQ3a',
    ruleId: Keys.TE_FORM_CURRENT_STATE,
    english: `I'm married.`,
    hiragana: `わたしは けっこん して います`,
    feedback: [
      ...teFormOngoingStateFeedback,
      {
        missing: 'して',
        advice: 'You must include して (you are not a marriage, you are married)'
      }
    ]
  },
  {
    id: 'EFiQWHH5z',
    ruleId: Keys.TE_FORM_CURRENT_STATE,
    english: `I have a car.`,
    hiragana: `わたしは くるま を もって います`,
    feedback: teFormOngoingStateFeedback
  },
  {
    id: 'cGxfCe1co',
    ruleId: Keys.TE_FORM_CURRENT_STATE,
    english: `I have a small home.`,
    hiragana: `わたしは ちさい うち を もって います`,
    feedback: teFormOngoingStateFeedback
  },
  {
    id: 'gu4JzfvlZ',
    ruleId: Keys.TE_FORM_CURRENT_STATE,
    english: `I live in Osaka`,
    hiragana: `わたしは おおさか に すんで います`,
    feedback: [
      ...teFormOngoingStateFeedback,
      {
        match: 'へ',
        advice: 'This should always be に when describing a place of residence'
      },
      {
        missing: 'すんで',
        advice: 'The て-form of live is `すんで`'
      }
    ]
  },
  {
    id: 'b9GjY6pQL',
    ruleId: Keys.TE_FORM_CURRENT_STATE,
    english: `I work at the hospital`,
    hiragana: `わたしは びょういん で はたらいて います`,
    feedback: [
      ...teFormOngoingStateFeedback,
      {
        missing: 'びょういん',
        advice: 'The correct spelling for hospital is `びょういん`'
      }
    ]
  },
  {
    id: 'AU7BAzYsy',
    ruleId: Keys.TE_FORM_CURRENT_STATE,
    english: `I know Mr. Yamada`,
    hiragana: `わたし は やまだ さん を して います`,
    feedback: teFormOngoingStateFeedback
  },
]
