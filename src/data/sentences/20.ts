import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

const defaultPlainFormFeedback =
  [
    {
      match: 'あります',
      advice: 'Don\'t use this in plain form (Use ある)'
    },
    {
      match: 'ません',
      advice: 'Don\'t use this in plain form (Use ない)'
    },
    {
      match: 'ました',
      advice: 'Don\'t use this in plain form (Use た)'
    },
    {
      match: 'です',
      advice: 'Don\'t use this in plain form (Use だ)'
    }
  ]

const instructions: string = 'Convert to Plain Form'

export default [
  {
    id: 'ID03z8WBW',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    hiragana: 'かく',
    english: 'かきます',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'かく',
        advice: 'The plain form of かきます is かく'
      }
    ]
  },
  {
    id: 'kii9dOo2m',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    hiragana: 'いく',
    english: 'いきます',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'いく',
        advice: 'The plain form of いきます is いく'
      }
    ]
  },
  {
    id: 'O9gotaXZ0',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    hiragana: 'ある',
    english: 'あります',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'ある',
        advice: 'The plain form of あります is ある'
      }
    ]
  },
  {
    id: '8K7KxsrBj',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    hiragana: 'できる',
    english: 'できます',
    instructions,
    feedback: defaultPlainFormFeedback
  },
  {
    id: 'IEh3lfjzK',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    hiragana: 'しらない',
    english: 'しります',
    instructions,
    feedback: defaultPlainFormFeedback
  },
  {
    id: 'rHy5UIsgu',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASU,
    hiragana: 'あした また くる',
    english: 'あした また きます',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'くる',
        advice: 'The plain form of きます is くる'
      }
    ]
  },
  {
    id: 'Czg9DUZ-H',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASEN,
    hiragana: 'かかない',
    english: 'かきません',
    instructions,
    feedback: defaultPlainFormFeedback
  },
  {
    id: 'Sj4jbCCV6',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASEN,
    hiragana: 'できない',
    english: 'できません',
    instructions,
    feedback: defaultPlainFormFeedback
  },
  {
    id: '64Sm0QKjP',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASEN,
    hiragana: 'きょう は なに も かわない',
    english: 'きょう は なに も かいません',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'かわない',
        advice: 'The ない-form of かいます is かわない'
      }
    ] 
  },
  {
    id: 'Ew8KGRvnt',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_IMASU,
    hiragana: 'ほん を よんで いる',
    english: 'ほん を よんで います',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'いる',
        advice: 'In plain form: います => いる'
      }
    ]
  },
  {
    id: '61w0cAMpo',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASHITA,
    hiragana: 'すこし つかれた',
    english: 'すこし つかれました',
    instructions,
    feedback: defaultPlainFormFeedback
  },
  {
    id: 'y2j3B6fOV',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_MASEN_DESHITA,
    hiragana: 'きのう にっき を かかなかった',
    english: 'きのう にっき を かきませんでした',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'かかなかった',
        advice: 'In these past tense cases, use [V.DictionaryForm] なかった'
      }
    ]
  },
  {
    id: 'pEmi6aqlZ',
    ruleId: Keys.PLAIN_FORM_VERB_CONVERSION_JYA_ARIMASEN_DESHITA,
    hiragana: 'きょう は やすみ じゃ ない',
    english: 'きょう は やすみ じゃ ありませんでした',
    instructions,
    feedback: defaultPlainFormFeedback
  },
  {
    id: 'IqRMTgXFU',
    ruleId: Keys.PLAIN_FORM,
    hiragana: 'すし を たべた こと が ある',
    english: 'すし を たべた こと が ありますか',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        match: 'たべて',
        advice: 'Ue the た form'
      }
    ]
  },
  {
    id: '8huOlpFlv',
    ruleId: Keys.PLAIN_FORM,
    hiragana: 'もう かえらなければ ならない',
    english: 'もう かえらなければ なりません か',
    instructions,
    feedback: defaultPlainFormFeedback,
    
  },
  {
    id: 'RDhCdDezp',
    ruleId: Keys.PLAIN_FORM,
    hiragana: 'あした わたし の うち へ あそびに こない',
    english: 'あした わたし の うち へ あそびに きませんか',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'こない',
        advice: 'The plain form of きません is こない'
      }
    ]
  },
  {
    id: 'kCx84RPYZ',
    ruleId: Keys.PLAIN_FORM,
    hiragana: 'あした とうきょう へ いく',
    english: 'I\'m off to Tokyo tomorrow',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        match: 'いきます',
        advice: 'Use dictionary verbs (いきます -> いく)'
      },
      {
        match: 'て',
        advice: 'Don\'t use て form here. It implies a future action'
      },
      {
        match: 'た',
        advice: 'Don\'t use た form here. It implies a past action'
      }
    ]
  },
  {
    id: 'rHfkmCVRt',
    ruleId: Keys.PLAIN_FORM,
    hiragana: 'まいにち いそがしい',
    english: 'I\'m busy every day',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'いそがしい',
        advice: 'Busy: いそがしい. Don\'t forget the final い.'
      }
    ]
  },
  {
    id: 'QYJfVT7JR',
    ruleId: Keys.PLAIN_FORM,
    hiragana: 'おきなわ へ いって こと が ない',
    english: '(I\'ve) never been to Okinawa',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        missing: 'こと',
        advice: 'An experience is denoted by こと'
      }
    ]
  },
  {
    id: 'UiYq59ngD',
    ruleId: Keys.PLAIN_FORM_QUESTIONS_DROP_KA,
    hiragana: 'まいにち かれ に でんわ する',
    english: 'Do you call your boyfriend every day?',
    instructions,
    feedback: defaultPlainFormFeedback,
  },
  {
    id: 'N0as2gJEB',
    ruleId: Keys.PLAIN_FORM_QUESTIONS_DROP_KA,
    hiragana: 'おちゃ のむ?',
    english: 'Want some tea?',
    instructions,
    feedback: [
      ...defaultPlainFormFeedback,
      {
        match: 'おちや',
        advice: 'Mis-spelling of おちゃ. The や is ちさい よ'
      }
    ]
  },
  {
    id: 'azFp6XfgF',
    ruleId: Keys.PLAIN_FORM_CONVERSION_NOUN,
    hiragana: 'やすみ だ',
    english: 'やすみ です',
    instructions,
    feedback: defaultPlainFormFeedback
  }
] as ISentence[]
