import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export const sentences: ISentence[] = [
  { // 5
    id: 'rY1CPcGcx7',
    ruleId: Keys.HE_DESTINATION,
    hiragana: '{city} へ いきます',
    english: 'I will go to {city}',
  },
  {
    id: '2q_bd1R_sB',
    ruleId: Keys.HE_DESTINATION,
    hiragana: '{city} へ いきました',
    english: 'I went to {city}'
  },
  {
    id: 'eyC100VycBV',
    ruleId: Keys.DE_TRANSPORT_METHOD,
    hiragana: 'でんしゃ で いきます',
    english: 'I will travel by train'
  },
]
