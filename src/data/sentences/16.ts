import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export const sentences: ISentence[] = [
  {
    id: '0zBWNMNpl',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: 'I ate lunch, then slept',
    hiragana: 'ひるごはん を たべて, ねました',
  },
  {
    id: 'nVk45hlVn',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: 'I will wake up at eight, have a bath, then go to school',
    hiragana: '8じに おきて、おふる を あびて, それからがこうに いきます',
  },
  {
    id: 'uNLqfYPIr',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: 'I ate lunch, bought some bread, then went home',
    hiragana: 'ひるごはん を たべて, パン を かいて, うち へ 帰りました',
  },
  {
    id: '6ACQAYwnN',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: `I went to school, studied kanji, then went home`,
    hiragana: `がっこう へ 行って, かんじ を 勉強 して, うちへ かえりました`,
  },
  {
    id: '92dKq6HaD',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: `I went to the library, borrowed a book, then met my friend`,
    hiragana: `としょかん へ いって, ほんをかりて, それから ともだち に あいました`,
  },
  {
    id: 'Ln-K3uvGH',
    ruleId: Keys.TE_FORM_VERB_JOIN_SENTENCES,
    english: `Last night, I listened to a CD, read a book, then slept`,
    hiragana: `きのう の ばん CD を きいて, ほん を よんで, ねました`,
  },
  {
    id: 'Bo53NlCln',
    ruleId: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    english: 'This dog is small and cute',
    hiragana: 'この いぬ は ちさくて, かわいい です',
  },
  {
    id: 'yjqL2rRWN',
    ruleId: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    english: 'This book is interesting and cheap',
    hiragana: 'この ほん は おもしろくて, やすい です',
  },
  {
    id: 'eoOT9l2na',
    ruleId: Keys.TE_FORM_II_ADJ_JOIN_SENTENCES,
    english: 'This applle is large and tasty',
    hiragana: 'この りんご は おおきくて おいしい です',
  },
  {
    id: 'gEXx_vRr2',
    ruleId: Keys.TE_FORM_NA_ADJ_JOIN_SENTENCES,
    english: 'Osaka is a quiet, beautiful city',
    hiragana: 'おおさか は しずか で, きれい な まち です',
  },
  {
    id: 'i8izwJyHP',
    ruleId: Keys.TE_FORM_NA_ADJ_JOIN_SENTENCES,
    hiragana: 'とうきょう は にぎやか で べんりな です',
    english: 'Tokyo is lively and convenient'
  },
]
