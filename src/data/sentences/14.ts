import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export const sentences: ISentence[] = [
  {
    id: '-MXusJdtK',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが この かんじ の よみかた を おしえて ください',
    english: 'Excuse me, could you please tell me how to read this kanji?',
    feedback: [
      {
        missing: 'よみかた',
        advice: 'The method pf reading is よみかた'
      },
      {
        match: 'かんじが',
        advice: 'This should be かんじ `の` ...'
      }
    ]
  },
  {
    id: 'Co9nj9ObN',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが しお を とって ください',
    english: 'Excuse me, could you please pass the salt?',
    feedback: [
      {
        missing: 'しお',
        advice: 'The word for salt is `しお`'
      }
    ]
  },
  {
    id: '8YSmEB5l9',
    ruleId: Keys.TE_FORM_PLEASE_DO_REQUESTING,
    hiragana: 'すみませんが もう すこし ゆっくり はなして ください',
    english: 'Excuse me, could you please speak more slowly?',
    feedback: [
      {
        match: 'ゆくり',
        advice: '(Spelling) ゆくり -> ゆっくり'
      },
      {
        match: 'をはなして',
        advice: 'The particle `を` should not be here: `slowly` is not an object.'
      }
    ]
  },
  {
    id: 'L5s1wYZiB',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'どうぞ、 たべて ください',
    english: 'Go ahead, Please eat.'
  },
  {
    id: 'llM08R-xt',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'どうぞ のんで ください',
    english: 'Go ahead, Please drink.'
  },
  {
    id: 'N-vBjN-tw',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'すわて ください',
    english: 'Please sit',
    feedback: [
      {
        missing: 'すわて',
        advice: 'The て-form of sit is `すわて`'
      }
    ]
  },
  {
    id: 'bSei7fMbZ',
    ruleId: Keys.TE_FORM_PLEASE_DO_INVITING,
    hiragana: 'はいって ください',
    english: 'Please come in (enter)',
    feedback: [
      {
        missing: 'はいって',
        advice: 'The て-form of enter is `はいって`'
      }
    ]
  },
  {
    id: 'AU_2O0Gp6',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'なまえ を かいて ください',
    english: 'Please write your name',
    feedback: [
      {
        missing: 'かいて',
        advice: 'The て-form of write is `かいて`'
      }
    ]
  },
  {
    id: 'jzjmLR4mX',
    ruleId: Keys.TE_FORM_PLEASE_DO_TELLING,
    hiragana: 'じゅうしょ を かいて ください',
    english: 'Please write your address',
    feedback: [
      {
        missing: 'かいて',
        advice: 'The て-form of write is `かいて`'
      },
      {
        missing: 'じゅうしょ',
        advice: 'The word for address is じゅうしょ'
      }
    ]
  },
  {
    id: 'GoH-Mvkuf',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'いま あめ が ふって います か',
    english: 'Is it raining right now?',
    feedback: [
      {
        missing: 'ふって',
        advice: 'The て-form of ふります is ふって'
      },
      {
        match: 'あります',
        advice: 'An event that is in progress should be denoted by います'
      }
    ]
  },
  {
    id: '9Xm8iJIml',
    ruleId: Keys.TE_FORM_IN_PROGRESS,
    hiragana: 'はい、いま ゆき が ふって います',
    english: 'Yes, it\'s snowing right now',
    feedback: [
      {
        match: 'あります',
        advice: 'An event that is in progress should be denoted by います'
      }
    ]
  },
  {
    id: 'yLUnUUglN',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'にもつ を もちましょうか',
    english: 'Can I carry your luggage?',
    feedback: [
      {
        match: 'が',
        advice: 'Use を as this denotes an action involving a subject (にもつ)'
      },
      {
        missing: 'もちましょうか',
        advice: 'The Presumptive form of もちます is もちましょう'
      }
    ]
  },
  {
    id: 'FwsDCtoHk',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'あつい です ね? まど を あけましょうか?',
    english: 'It\'s hot huh? Should I open the window?',
    feedback: [
      {
        missing: 'まど',
        advice: 'The word for window is まど'
      },
      {
        missing: 'あけましょうか',
        advice: 'The Presumptive form of あけます is あけましょう'
      }
    ]
  },
  {
    id: 'RSq5UXEX8',
    ruleId: Keys.TE_FORM_SHALL_I,
    hiragana: 'さむい です ね? まど を しめましょう か?',
    english: 'It\'s cold huh? Should I close the window?',
    feedback: [
      {
        missing: 'まど',
        advice: 'The word for window is まど'
      },
      {
        missing: 'しめましょう',
        advice: 'The Presumptive form of しめます is しめましょう'
      }
    ]
  },
]