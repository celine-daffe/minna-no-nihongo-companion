import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: '1bI47uroJ',
    ruleId: Keys.TOKI_NA_ADJ,
    hiragana: 'ひま な とき うちへ あそび に きませんか',
    english: 'If you have sme free time, come visit me'
  },
] as ISentence[]