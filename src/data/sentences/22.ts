import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

const missingJikan = [
  {
    mising: 'じかん',
    advice: 'The word you wanted was じかん'
  }
]

export default [
  {
    id: '4OzUslquZ',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'きれい な ひと です',
    english: 'A beautiful person'
  },
  {
    id: 'ivfLBaNMV',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'おもしろい だった ひと です',
    english: 'A person who was interesting',
    feedback: [
      {
        missing: 'だった',
        advice: 'Use `だった` as this is a past tense いい adjective (て)'
      }
    ]
  },
  {
    id: 'C9iHVVwVw',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'おもしろい ひと です',
    english: 'An interesting person',
    feedback: [
      {
        match: 'な',
        advice: 'おもしろい is an い adjective, no need to add な'
      }
    ]
  },
  {
    id: 'kizK2gL86',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'しんせつで、きれい な ひと',
    english: 'A kind, beautiful person'
  },
  {
    id: 'HTrgTSaIa',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'みどり ずぼん を はいて いる ひと です',
    english: 'The person wearing the green pants',
    feedback: [
      {
        missing: 'いる',
        advice: 'You need to say that they are wearing the item :/'
      }
    ]
  },
  {
    id: 'TkjwXxxl8',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'あかい ぼうし を かぶって いる ひと です',
    english: 'The person wearing the red hat',
    feedback: [
      {
        missing: 'かぶって',
        advice: 'The て-form of wearing (head) is かぶって'
      },
      {
        missing: 'いる',
        advice: 'You need to say that they are wearing the item :/'
      }
    ]
  },
  {
    id: 'Wy1GvEtFV',
    ruleId: Keys.NOUN_MODIFICATION_DESCRIPTIONS,
    hiragana: 'めがね を かけて いる ひと です',
    english: 'The person wearing the glasses',
    feedback: [
      {
        missing: 'かけて',
        advice: 'The て-form of wearing (face) is かけて'
      }
    ]
  },
  {
    id: 'MTaSBAxnY',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'これは としょうかん で とった しゃしん です',
    english: 'This is a photo I took at the library',
    feedback: [
      {
        match: 'とって',
        advice: 'This should be in た-form (たって)'
      }
    ]
  },
  {
    id: '2IVXzoc7I',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'これは おおさか で かいた え です',
    english: 'This is a painting I painted in Osaka',
    feedback: [
      {
        missing: 'かいた',
        advice: 'The た-form of paint/draw is かいた'
      }
    ]
  },
  {
    id: 'hibyi2NdN',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'これは ほんや で かった ほん です',
    english: 'This is a book I bought at the bookshop'
  },
  {
    id: '1PlujCrmF',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'これは なら で ともだち が くれた ぼうし です',
    english: 'This is the hat my friend gave me in Nara',
    feedback: [
      {
        missing: 'くれた',
        advice: 'Give (polite) is くれます. The た-form is くれた'
      }
    ]
  },
  {
    id: 'EF05KKlWc',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'きのう わたしたちが いった おてら は きれいで, しずかでした',
    english: 'Yesterday, my friends and I went to a temple which was beatiful and quiet'
  },
  {
    id: 'VYj6ZnZBu',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'れんしょう つくって みそ',
    english: 'The miso I made last week',
    feedback: [
      {
        missing: 'つくって',
        advice: 'The た-form of make/made is つくって'
      }
    ]
  },
  {
    id: 'abjJ4qSUn',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'きのう かった しんぶん',
    english: 'The newspaper I bought yesterday',
    feedback: [
      {
        missing: 'かった',
        advice: 'The た-form of buy is かった'
      }
    ]
  },
  {
    id: 'nr7ukVHIh',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'けさ かりて ほん',
    english: 'The book I borrowed this morning',
    feedback: [
      {
        missing: 'かりて',
        advice: 'The た-form of borrow is かりて'
      }
    ]
  },
  { // @TODO it would be nice to match either via [どこにありますか|どこですか]
    id: 'mlp_q74sW',
    ruleId: Keys.TA_FORM_VERB_AT_PLACE,
    hiragana: 'きのう かった ほん は どこ に ありますか',
    english: 'Where is the book I bought yesterday?',
    feedback: [
      {
        missing: 'かった',
        advice: 'The た-form of buy is かった'
      }
    ]
  },
  {
    id: 'mMVRYp16G',
    ruleId: Keys.CANT_DO_YAKUSOKU,
    hiragana: 'いいえ, ともだち に あう やくそく が あります',
    english: 'No, I can\'t, I\'m meeting my friend'
  },
  {
    id: 'rOsjbEgoj',
    ruleId: Keys.CANT_DO_YAKUSOKU,
    hiragana: 'いいえ, なら へ いる やくそく が あります',
    english: 'No, I can\'t, I\'m going to Nara'
  },
  {
    id: 'xDahJQxnJ',
    ruleId: Keys.CANT_DO_YAKUSOKU,
    hiragana: 'いいえ, ともだち と えいが を みる やくそく が あります',
    english: 'No, I can\'t, I\'m watching a movie with my friend'
  },
  {
    id: 'kMFyCYXs3',
    ruleId: Keys.CANT_DO_YAKUSOKU,
    hiragana: 'いいえ, いぬ と こえん へ いる やくそく が あります',
    english: 'No, I can\'t, I\'m going to the park with my dog'
  },
  {
    id: 'vhGWSmEoi',
    ruleId: Keys.CANT_DO_JIKAN_GA_ARIMASEN,
    hiragana: 'いいえ, しごと へ いる じかん が ありません',
    english: 'No, I have to go to work, I don\'t have time',
    feedback: missingJikan
  },
  {
    id: 'P0NMlCn0N',
    ruleId: Keys.CANT_DO_JIKAN_GA_ARIMASEN,
    hiragana: 'いいえ, ほん を よむ じかん が ありません',
    english: 'No, I don\'t read books, I don\'t have time',
    feedback: [
      ...missingJikan,
      {
        missing: 'よむ',
        advice: 'The dictionary form of read is よむ'
      }
    ]
  },
  {
    id: 'rTuXxNkQk',
    ruleId: Keys.CANT_DO_JIKAN_GA_ARIMASEN,
    hiragana: 'いいえ, あさごはん を たべる, じかん が ありません',
    english: 'No, I don\'t eat breakfast, I don\'t have time',
    feedback: missingJikan
  },
  {
    id: 'XTCfgAYmB',
    ruleId: Keys.CANT_DO_JIKAN_GA_ARIMASEN,
    hiragana: 'いいえ, こえん へ いる じかん が ありませんでした',
    english: 'No, I didn\'t go to the park, I didn\'t have time',
    feedback: [
      ...missingJikan,
      {
        missing: 'でした',
        advice: 'Be careful of past tense items'
      }
    ]
  },
  {
    id: 'O9emKNoDy',
    ruleId: Keys.CANT_DO_JIKAN_GA_ARIMASEN,
    hiragana: 'いいえ, しんぶん を よむ じかん が ありませんでした',
    english: 'No, I didn\'t read the newspaper, I didn\'t have time',
    feedback: [
      ...missingJikan,
      {
        missing: 'でした',
        advice: 'Be careful of past tense items'
      }
    ]
  }
] as ISentence[]