import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export const sentences: ISentence[] = [
  {
    id: 'P1J9sKb6G',
    ruleId: Keys.NI_TIME,
    hiragana: '6 じかん に おきます',
    english: 'I will wake up at 6',
    feedback: [
      {
        missing: 'に',
        advice: 'Use the `に` particle to denote the time of an event'
      }
    ]
  },
  {
    id: 'r3totL9JPzz',
    ruleId: Keys.MADE_KARA_BEGIN_END,
    hiragana: 'うち から えき まで',
    english: 'From my house to the train station'
  },
  {
    id: 'rSqWZZiffr',
    ruleId: Keys.TO_COORDINATE_RELATION,
    hiragana: '{place} の やすみ は どようび と にちようび です',
    english: '{place} is closed on Saturday and Sunday'
  },
]