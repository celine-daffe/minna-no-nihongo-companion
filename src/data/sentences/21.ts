import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'vdSIY5RLx',
    ruleId: Keys.I_THINK_POSITIVE_CONJECTURE,
    hiragana: 'わたしは あした は あめが ふる と おもいます',
    english: 'I think it will rain tomorrow',
    feedback: [
      {
        missing: 'ふる',
        advice: 'The dictionary form of fall is ふる'
      }
    ]
  },
  {
    id: 'SB0IRglUB',
    ruleId: Keys.I_THINK_NEGATIVE_CONJECTURE,
    hiragana: 'たぶん もう かえった と おもいます',
    english: 'I think he may have already gone home',
  }
] as ISentence[]