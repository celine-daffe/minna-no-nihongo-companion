import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export default [
  {
    id: 'dqTnfoC2e',
    ruleId: Keys.TA_FORM_ACTIVITY_EXPERIENCED,
    hiragana: 'なに',
    english: '(Empty)',
  }
] as ISentence[]