import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

const missing = {
  agemashita: [
    {
      missing: 'あげます',
      advice: 'If the topic of the subject (excluding 私) receieves something, use あげます'
    }
  ],
  kuremashita: [
    {
      missing: 'くれました',
      advice: 'If you have received something or benefitted from an action, use くれました'
    }
  ]
}

export default [
  {
    id: 'q4IP_wmGj',
    ruleId: Keys.GIVING_RECEIVING_AGEMASHITA,
    hiragana: 'わたし は ともだち に ほん を あげました',
    english: 'I gave my friend a book',
    feedback: missing.agemashita
  },
  {
    id: 'P6vhCG_PD',
    ruleId: Keys.GIVING_RECEIVING_AGEMASHITA,
    hiragana: 'わたし は おとうと に ぼうし を あげました',
    english: 'I gave my brother a hat',
    feedback: missing.agemashita
  },
  {
    id: 'sUhu9FpYK',
    ruleId: Keys.GIVING_RECEIVING_AGEMASHITA,
    hiragana: 'わたし の はは は おばあちゃん に はな を あげました',
    english: 'My mother gave the old lady flowers',
    feedback: missing.agemashita
  },
  {
    id: 'nQNeVFr6C',
    ruleId: Keys.GIVING_RECEIVING_KUREMASHITA,
    hiragana: 'せんせい は わたし に ほん を くれました',
    english: 'The teacher gave me a book',
    feedback: missing.kuremashita
  },
  {
    id: 'dETTa50rE',
    ruleId: Keys.GIVING_RECEIVING_KUREMASHITA,
    hiragana: 'おじいちゃん は わたし に ひゃく えん くれました',
    english: 'The old man gave me 100 yen',
    feedback: missing.kuremashita
  },
  {
    id: 'S_D5eSA2u',
    ruleId: Keys.GIVING_RECEIVING_TE_KUREMASU,
    hiragana: 'ちち は おかね を かして くれました',
    english: 'Dad lent me some money',
    feedback: missing.kuremashita
  },
  {
    id: 'RuKRc1Tsg',
    ruleId: Keys.GIVING_RECEIVING_TE_KUREMASU,
    hiragana: 'わたし の ともだち は ほん を かいて くれました',
    english: 'My friend bought me a book',
    feedback: missing.kuremashita
  },
  {
    id: 'BHjrA3URe',
    ruleId: Keys.GIVING_RECEIVING_TE_KUREMASU,
    hiragana: 'わたし の ともだち は じてんしゃ を なおして くれました',
    english: 'My friend fixed by bicycle',
    feedback: missing.kuremashita
  },
  {
    id: 'RZQAvMSRg',
    ruleId: Keys.GIVING_RECEIVING_TE_KUREMASU,
    hiragana: 'わたし の いもうと は おおさか を あんあい して くれました',
    english: 'My sister showed me around Oosaka',
    feedback: missing.kuremashita
  },
  {
    id: 'G-2TkCVTk',
    ruleId: Keys.GIVING_RECEIVING_TE_KUREMASU,
    hiragana: 'わたし の あね は すし を つくて くれました',
    english: 'My older sister made me some sushi',
    feedback: missing.kuremashita
  },
] as ISentence[]