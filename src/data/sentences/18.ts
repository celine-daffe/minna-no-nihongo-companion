import { ISentence } from '../../lib'
import { Keys } from '../grammar/keys'

export const sentences: ISentence[] = [
  {
    id: 'A1WhijIFY',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: 'わたし は にほんご が できます',
    english: 'I can speak Japanese',
    feedback: [
      {
        match: 'にほんごを',
        advice: 'In the できます form, `か` preceeds できます, not `を`'
      }
    ]
  },
  {
    id: 'rPMzrjj_A',
    ruleId: Keys.DEKIMASU_NOUN,
    hiragana: 'わたし は にほんご が できません',
    english: 'I can\'t speak japanese'
  },
  {
    id: 'MxY85pPvs',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は おんがく を きく こと です',
    english: 'My hobby is listening to music',
    feedback: [
      {
        missing: 'きく',
        advice: `The dictionary form of listen is 'きく'`
      }
    ]
  },
  {
    id: 'd0kIHMgQp',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は ほん を よむ こと です',
    english: 'My hobby is reading books',
    feedback: [
      {
        missing: 'よむ',
        advice: `The dictionary form of read is 'よむ'`
      }
    ]
  },
  {
    id: 'P10R4gCc0',
    ruleId: Keys.HOBBIES_VERB_FORM,
    hiragana: 'わたし の しゅみ は くるま を なお する こと です',
    english: 'My hobby is reparing cars',
    feedback: [
      {
        missing: 'なお',
        advice: `The dictionary form of repair is 'なお すろ'`
      }
    ]
  },
]