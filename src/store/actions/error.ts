import { ActionCreator } from 'redux'
import { ErrorAction, IErrorAction } from '../../interfaces'

type IActionCreator = ActionCreator<IErrorAction>

export const setError: IActionCreator = (payload: string) => ({
  type: ErrorAction.SET_ERROR,
  payload
})
