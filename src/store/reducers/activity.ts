import {
  unmaskGrammarInRuleItem,
  loadOptions,
} from '../../lib'

import {
  ActivityAction,
  IQuizActivity,
  IQuestion,
  IGrammarItem,
  IQuestionResult,
} from '../../interfaces'

import { NoActivity } from '../../activities/noActivity'

export interface IActivityState {
  currentActivity: IQuizActivity
  options: IOptionState
  running: boolean
  questions: Iterator<IQuestion>
  currentQuestion: IQuestion
  submittedAnswer: string[]
  correct: boolean | null
  complete: boolean
  results: IQuestionResult[]
}

export interface IOptionState {
  [key: string]: any
}

export const initialState: IActivityState = {
  currentActivity: new NoActivity(),
  options: loadOptions(),
  running: false,
  questions: [][Symbol.iterator](),
  currentQuestion: {
    id: '',
    question: '',
    options: [],
    answer: ['']
  },
  submittedAnswer: [],
  correct: null,
  complete: false,
  results: []
}

export const reducer = (
  state: IActivityState = initialState,
  action: {
    type: ActivityAction
    payload?: any
  }
): IActivityState => {
  switch (action.type) {
    case ActivityAction.SET_ACTIVITY:
      return {
        ...state,
        currentActivity: action.payload,
        running: false
      }
    case ActivityAction.SET_OPTION:
      return {
        ...state,
        options: {
          ...state.options,
          [action.payload.activityId]: {
            ...state.options[action.payload.activityId],
            [action.payload.optionKey]: action.payload.optionValue
          }
        }
      }
    case ActivityAction.START:
      const questions = state.currentActivity.getQuestions(state.options[state.currentActivity.type])

      return {
        ...state,
        running: true,
        questions,
        currentQuestion: questions.next().value
      }
    case ActivityAction.SUBMIT_ANSWER:
      const result = state.currentActivity.evaluateAnswer(
        action.payload, state.currentQuestion
      )

      return {
        ...state,
        submittedAnswer: state.submittedAnswer.concat(action.payload),
        correct: result.correct
      }
    case ActivityAction.SKIP_QUESTION:
      return {
        ...state,
        submittedAnswer: [],
        correct: false
      }
    case ActivityAction.UNMASK_QUESTION:
      return {
        ...state,
        currentQuestion: {
          ...state.currentQuestion,
          question: unmaskGrammarInRuleItem(
            state.submittedAnswer,
            state.currentQuestion.question as IGrammarItem
          )
        },
      }
    case ActivityAction.RECORD_RESULT:
      return {
        ...state,
        results: [
          ...state.results,
          {
            question: state.currentQuestion,
            submittedAnswer: state.submittedAnswer,
            correct: state.correct as boolean,
            feedback: !state.correct
              ? state.currentActivity.getFeedback(
                state.currentQuestion,
                state.submittedAnswer
              )
              : null
          }
        ]
      }
    case ActivityAction.NEXT_QUESTION:
      // The answer may be incomplete
      if (state.correct === null) {
        return state
      }

      return {
        ...state,
        correct: null,
        currentQuestion: action.payload,
        submittedAnswer: []
      }
    case ActivityAction.COMPLETE:
      return {
        ...state,
        running: false,
        complete: true
      }
    case ActivityAction.RESET:
      return {
        ...initialState,
        currentActivity: state.currentActivity,
        questions: state.currentActivity.getQuestions(state.options[state.currentActivity.type]),
        options: state.options,
        running: true
      }
    default:
      return state
  }
}
