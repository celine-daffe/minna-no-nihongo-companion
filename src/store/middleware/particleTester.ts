import { Store } from 'redux'
import { ActivityAction, ActivityType } from '../../interfaces'
import { unmaskQuestion } from '../actions/activity'

const ANSWER_INCOMPLETE = null

export const particleTesterMiddleware = (store: Store) => (next: any) => (action: any) => {
  const { activity } = store.getState()

  if (activity.currentActivity.type !== ActivityType.TEST_GRAMMAR) {
    return next(action)
  }

  switch (action.type) {
    /**
     * We have a custom action we dispatch which 'unmasks' the particles in the question
     */
    case ActivityAction.SUBMIT_ANSWER:
      next(action)
      return store.dispatch(unmaskQuestion())

    /**
     * We can have multiple particle options, we don't necessary want to get the next question
     * and move on after submission. Check whether the answer is incomplete before acting
     */
    case ActivityAction.RECORD_RESULT:
    case ActivityAction.NEXT_QUESTION:
    case ActivityAction.COMPLETE:
      return activity.correct === ANSWER_INCOMPLETE ? null : next(action)
    default:
      return next(action)
  }
}
