import { Store } from 'redux'
import { OptionAction, IOptionAction, IOptionItem } from '../../interfaces'
import { persistOption } from '../../lib/storage'

/**
 * This middleware captures any settings changes and updates the localstorage values 
 */
export const persistOptionsMiddleware = (store: Store) => (next: any) => (action: IOptionAction) => {
  const { type, payload } = action

  if (type !== OptionAction.SET_OPTION) {
    return next(action)
  }

  persistOption(
    payload as IOptionItem
  )

  return next(action)
}
