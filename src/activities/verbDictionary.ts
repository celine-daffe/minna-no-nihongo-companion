import {
  ActivityType,
  IActivity,
} from '../interfaces'

export class VerbDictionary implements IActivity {
  public title = 'Verb Dictionary'
  public subtitle = 'Find Verbs by chapter or word'
  public type = ActivityType.DICT_VERBS
}
