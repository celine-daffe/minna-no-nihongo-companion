import {
  ActivityType,
  IActivity,
} from '../interfaces'

export class AdjectiveDictionary implements IActivity {
  public title = 'Adjective Dictionary'
  public subtitle = 'Find Adjectives by chapter or word'
  public type = ActivityType.DICT_ADJECTIVES
}
