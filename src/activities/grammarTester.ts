import {
  getGrammarQuizQuestions,
  checkAnswer,
} from '../lib'

import {
  ActivityType,
  IConversationItem,
  IGrammarTesterOptions,
  IQuizActivity,
  IGrammarTesterQuestion,
} from '../interfaces'

import {
  numberOfQuestionsOption,
  chapterRangeOptions,
} from '../data/options'

import { numberedArrayItems } from '../utils'

export class GrammarTester implements IQuizActivity {
  public type = ActivityType.TEST_GRAMMAR
  public title = 'Grammar'
  public subtitle = 'Select the correct particle to insert into a hiragana sentence'
  public options = [...chapterRangeOptions, numberOfQuestionsOption]

  public currentAnswer: string[] = []

  public evaluateAnswer = (answer: string[], question: IGrammarTesterQuestion) => {
    let result
    this.currentAnswer = this.currentAnswer.concat(answer)

    if (this.currentAnswer.length === question.answer.length) {
      result = checkAnswer(this.currentAnswer, question)
      this.currentAnswer = []
    }

    return result ? result : { correct: null }
  }

  public getQuestions = (
    options: IGrammarTesterOptions
  ): Iterator<IGrammarTesterQuestion> => {
    const result = getGrammarQuizQuestions(options)

    const items = numberedArrayItems(
      result.questions
    )

    return items[Symbol.iterator]()
  }

  public getFeedback = (question: IGrammarTesterQuestion, submittedAnswer: string[]) => null

  public renderQuestion = (question: IGrammarTesterQuestion) => {
    if (
      typeof question.question === "object" &&
      Array.isArray(question.question.hiragana)
    ) {
      return (question.question.hiragana as IConversationItem[])
        .map(h => h.line)
        .join('<br>')
    }

    return question.question.hiragana as string
  }
}
