import { getVerbConjugationQuestions } from '../lib'

import {
  ActivityType,
  IQuizActivity,
  IVerbConjugationTesterOptions,
  IVerbConjugationTesterQuestion,
} from '../interfaces'

import {
  numberOfQuestionsOption,
  verbFormOptions,
} from '../data/options'

import { equalArrays, numberedArrayItems } from '../utils'

export class VerbConjugationTester implements IQuizActivity {
  public type = ActivityType.TEST_VERB_CONJUGATION
  public title = 'Verb Conjugation'
  public subtitle = 'Test changing verbs between forms'
  public options = [numberOfQuestionsOption, ...verbFormOptions]

  public evaluateAnswer = (answer: string[], question: IVerbConjugationTesterQuestion) => ({
    correct: equalArrays(answer, question.answer)
  })

  public getQuestions = (
    options: IVerbConjugationTesterOptions
  ): Iterator<IVerbConjugationTesterQuestion> => {
    const items = numberedArrayItems(
      getVerbConjugationQuestions(options)
    )

    return items[Symbol.iterator]()
  }

  public getFeedback = (question: IVerbConjugationTesterQuestion, submittedAnswer: string[]) => null

  public renderQuestion = (question: IVerbConjugationTesterQuestion) => {
    return question.question as string
  }
}
