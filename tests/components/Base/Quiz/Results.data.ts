export const resultWithNoFeedback = {
  "question": {
    "id": "TSFmbnndF",
    "answer": [
      "どうぞ たくさん たべて ください"
    ],
    "question": "Go ahead. Please eat as much as you want",
    "options": [
      ""
    ],
    "meta": {
      "rule": {
        "id": 14.33,
        "particles": [
          "ください"
        ],
        "rule": "[Invitation]...[V.TeForm] ください",
        "explanation": "This pattern is more polite, and is used to invite someone to do something specific"
      },
      "sentence": {
        "id": "loJ1vdak3",
        "ruleId": 14.33,
        "hiragana": "どうぞ たくさん たべて ください",
        "english": "Go ahead. Please eat as much as you want"
      }
    },
    "questionNumber": 1
  },
  "submittedAnswer": [
    "どうぉ"
  ],
  "correct": false,
  "feedback": null
}