import 'jest'
import * as React from 'react'
import Question from '../../../src/components/GrammarTester/Question'
import { IGrammarItem } from '../../../src/interfaces'
import * as Enzyme from 'enzyme'
import * as Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({
  adapter: new Adapter(),
})

describe('Compnent:GrammarTester:Question', () => {
  it('correctly renders a string question', () => {
    const ruleItemFixture: IGrammarItem = {
      id: 'abc',
      ruleId: 0,
      hiragana: '京都 {へ} 行きます'
    }

    const wrapper = Enzyme.mount(
      <Question ruleItem={ruleItemFixture} />
    )

    expect(wrapper.find('Sentence').length).toBe(1)
    expect(wrapper.find('span').length).toBe(1)
  })

  it('correctly renders a conversation question', () => {
    const ruleItemFixture: IGrammarItem = {
      id: 'abc',
      ruleId: 0,
      hiragana: [
        {
          line: 'first line',
        },
        {
          line: 'second line'
        }
      ]
    }

    const wrapper = Enzyme.mount(
      <Question ruleItem={ruleItemFixture} />
    )

    expect(wrapper.find('Conversation').length).toBe(1)
    expect(wrapper.find('span').length).toBe(2)
  })
})