import 'jest'
import { ErrorAction as Actions } from '../../../src/interfaces'
import { reducer } from '../../../src/store/reducers/error'

describe('reducer:error', () => {
  it('sets an error', () => {
    const result = reducer(undefined, { type: Actions.SET_ERROR, payload: 'An error occurred' })
    expect(result.error).toEqual('An error occurred')
  })
})
