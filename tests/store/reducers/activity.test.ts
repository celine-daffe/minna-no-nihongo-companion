import 'jest'
import { ActivityAction as Actions, ActivityType } from '../../../src/interfaces'
import { initialState, reducer } from '../../../src/store/reducers/activity'
import { VerbConjugationTester } from '../../../src/activities/verbConjugationTester'

describe('reducer:activity', () => {
  it('sets an activity', () => {
    const activity = new VerbConjugationTester()

    const result = reducer(initialState, { type: Actions.SET_ACTIVITY, payload: activity })

    expect(result.currentActivity).toEqual(activity)
    expect(result.running).toEqual(false)
    expect(result.currentActivity.type).toBe(ActivityType.TEST_VERB_CONJUGATION)
  })

  it('should reset the results on a reset', () => {
    const result = reducer(undefined, { type: Actions.RESET, payload: null })

    expect(result.results).toEqual([])
  })

  it('should not reset the options on a reset', () => {
    const nextState = reducer(undefined, {
      type: Actions.SET_OPTION,
      payload: {
        activityId: 1,
        optionKey: "MIN_CHAPTER",
        optionValue: 10
      }
    })

    const result = reducer(nextState, { type: Actions.RESET, payload: null })

    expect(result.options["1"]).toEqual({
      "MIN_CHAPTER": 10,
      "NUM_QUESTIONS": 10
    })
  })

  it('Calling NEXT_QUESTION with a null (incomplete) correct value should immediately return', () => {
    const nextState = reducer(initialState, { type: Actions.NEXT_QUESTION })
    expect(nextState).toEqual(initialState)
  })

  it('completing an activity should set running to false and cmplete to true', () => {
    const nextState = reducer(undefined, { type: Actions.COMPLETE })

    expect(nextState.complete).toEqual(true)
    expect(nextState.running).toEqual(false)
  })

  it('sets a single option', () => {
    const state = reducer(undefined, {
      type: Actions.SET_OPTION,
      payload: {
        activityId: 1,
        optionKey: "MIN_CHAPTER",
        optionValue: 10
      }
    })

    expect(state.options["1"]).toEqual({
      "MIN_CHAPTER": 10,
      "NUM_QUESTIONS": 10
    })
  })

  it('sets an option and merges correctly', () => {
    const activityId = 1

    const state = reducer(undefined, {
      type: Actions.SET_OPTION,
      payload: {
        activityId,
        optionKey: "MIN_CHAPTER",
        optionValue: 14
      }
    })

    expect(state.options[activityId]).toEqual({
      "MIN_CHAPTER": 14,
      "NUM_QUESTIONS": 10
    })

    const nextState = reducer(state, {
      type: Actions.SET_OPTION,
      payload: {
        activityId,
        optionKey: "MAX_CHAPTER",
        optionValue: 18
      }
    })

    expect(nextState.options[activityId]).toEqual({
      "MIN_CHAPTER": 14,
      "MAX_CHAPTER": 18,
      "NUM_QUESTIONS": 10
    })
  })

  it('should request the correct number of questions from the currentActivity when the option has been set', () => {
    // Set the activity
    const activity = new VerbConjugationTester()
    const afterActivitySetState = reducer(initialState, { type: Actions.SET_ACTIVITY, payload: activity })

    expect(afterActivitySetState.currentActivity.type).toBe(ActivityType.TEST_VERB_CONJUGATION)

    const afterOptionsSetState = reducer(afterActivitySetState, {
      type: Actions.SET_OPTION,
      payload: {
        activityId: ActivityType.TEST_VERB_CONJUGATION,
        optionKey: "NUM_QUESTIONS",
        optionValue: 8
      }
    })

    spyOn(activity, 'getQuestions').and.returnValue([][Symbol.iterator]())

    // start the activity
    reducer(afterOptionsSetState, { type: Actions.START })

    expect(activity.getQuestions).toHaveBeenCalledWith({
      NUM_QUESTIONS: 8
    })
  })
})
