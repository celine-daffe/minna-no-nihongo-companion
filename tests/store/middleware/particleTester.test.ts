import 'jest'
import { ActivityAction, ActivityType } from '../../../src/interfaces'
import { particleTesterMiddleware } from '../../../src/store/middleware/particleTester'
import { create } from './utils'

const createMiddleware = (state: any) => create(particleTesterMiddleware, state)

describe('middleware:particleTester', () => {
  it('dispatches the next action when the current activity is not TEST_GRAMMAR', () => {
    const { next, invoke } = createMiddleware(
      {
        activity: {
          currentActivity: {
            type: ActivityType.NONE
          }
        }
      })

    const action = { type: ActivityAction.START }
    invoke(action)
    expect(next).toHaveBeenCalledWith(action)
  })

  it(`dispatches the next action when the current activity is TEST_GRAMMAR,
    but we are not dispatching SUBMIT_ANSWER`, () => {
      const { next, invoke } = createMiddleware(
        {
          activity: {
            currentActivity: {
              type: ActivityType.TEST_GRAMMAR
            }
          }
        })

      const action = { type: ActivityAction.START }
      invoke(action)
      expect(next).toHaveBeenCalledWith(action)
    })

  it(`dispatches the next action when the current activity is TEST_GRAMMAR,
    and are are dispatching SUBMIT_ANSWER, and the answer is complete`, () => {
      const { store, next, invoke } = createMiddleware({
        activity: {
          currentActivity: {
            type: ActivityType.TEST_GRAMMAR
          }
        }
      })

      const action = {
        type: ActivityAction.SUBMIT_ANSWER
      }

      invoke(action)
      expect(next).toHaveBeenCalledWith(action)
      expect(store.dispatch).toHaveBeenCalled()
    })

  it(`dispatches the next action when the current activity is TEST_GRAMMAR,
    we are are dispatching RECORD_RESULT, and the answer is complete`, () => {
      const { next, invoke } = createMiddleware({
        activity: {
          correct: true,
          currentActivity: {
            type: ActivityType.TEST_GRAMMAR
          }
        }
      })

      const action = {
        type: ActivityAction.RECORD_RESULT
      }

      invoke(action)
      expect(next).toHaveBeenCalledWith(action)
    })

  it(`does not dispatch the next action when the current activity is TEST_GRAMMAR,
    we are are dispatching NEXT_QUESTION, and the answer is incomplete`, () => {
      const { next, invoke } = createMiddleware({
        activity: {
          correct: null,
          currentActivity: {
            type: ActivityType.TEST_GRAMMAR
          }
        }
      })

      const action = {
        type: ActivityAction.NEXT_QUESTION
      }

      invoke(action)
      expect(next).not.toHaveBeenCalled()
    })
})
