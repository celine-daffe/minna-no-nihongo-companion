import { Middleware, Store } from "redux"

export const create = (middleware: Middleware, state = {}) => {
  const store = {
    getState: jest.fn(() => (state)),
    dispatch: jest.fn()
  }

  const next = jest.fn()

  const invoke = (action: any) => middleware((store as unknown) as Store)(next)(action)

  return { store, next, invoke }
}