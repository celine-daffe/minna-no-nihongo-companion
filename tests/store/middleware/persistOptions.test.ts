import 'jest'
import * as sinon from 'sinon'
import * as storage from '../../../src/lib/storage'
import { persistOptionsMiddleware } from '../../../src/store/middleware/persistOptions'
import { ActivityAction, OptionAction } from '../../../src/interfaces'
import { create } from './utils'

const createMiddleware = (state: any) => create(persistOptionsMiddleware, state)

describe('middleware:persistOptions', () => {
  it('dispatches the next action when the action is not SET_OPTION', () => {
    const { next, invoke } = createMiddleware({})

    const action = { type: ActivityAction.START }
    invoke(action)

    expect(next).toHaveBeenCalledWith(action)
  })

  it('makes a call to persist the option then the action is SET_OPTION', () => {
    sinon.mock(storage).expects('persistOption')

    const { next, invoke } = createMiddleware({})

    const action = { type: OptionAction.SET_OPTION }
    invoke(action)

    expect(next).toHaveBeenCalledWith(action)
  })
})