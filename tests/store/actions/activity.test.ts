import "jest"
import * as actions from '../../../src/store/actions/activity'
import { ActivityAction } from '../../../src/interfaces/activity'

describe('actions:error', () => {
  it('startActivity', () => {
    expect(actions.startActivity()).toEqual({
      type: ActivityAction.START
    })
  })
})