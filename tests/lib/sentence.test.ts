import 'jest'
import {
  getSentenceQuestionItems,
  compareSentences,
  getSentenceQuestionFeedback,
  matchSpacing
} from '../../src/lib/sentence'

import {
  questionWithMissingFeedback,
  questionWithMatchFeedback,
  questionWithNoFeedback
} from './sentence.data'

describe('lib:sentences', () => {
  describe('Data Validation', () => {
    it('should have no duplicate ids', () => {
      const ids = getSentenceQuestionItems({
        NUM_QUESTIONS: 1000,
        MIN_CHAPTER: 1,
        MAX_CHAPTER: 25
      }).questions.map(q => q.id)

      const validate = (): boolean => {
        const foundIds: string[] = []

        ids.map((id: string) => {
          if (foundIds.indexOf(id) > -1) {
            throw new Error("Duplicate ID: " + id)
          }

          foundIds.push(id)
        })

        return true
      }

      expect(() => validate()).not.toThrowError()
    })
  })

  describe('getSentenceQuestionItems', () => {
    it('builds sentences correctly', () => {
      const questions = getSentenceQuestionItems({
        NUM_QUESTIONS: 10,
        MIN_CHAPTER: 1,
        MAX_CHAPTER: 25
      }).questions

      questions.map((question) => {
        expect(question.question).not.toContain(['{', '}'])
        expect(question.answer).not.toContain(['{', '}'])
      })
    })

    it('filters by chapter range correctly', () => {
      const questions = getSentenceQuestionItems({
        NUM_QUESTIONS: 10,
        MIN_CHAPTER: 11,
        MAX_CHAPTER: 20
      }).questions

      questions.map((question) => {
        expect(question.meta.rule.id).toBeGreaterThan(11)
        expect(question.meta.rule.id).toBeLessThan(21)
      })
    })
  })

  describe('compareSentences', () => {
    it('returns false for a non-match', () => {
      expect(compareSentences('a', 'b').correct).toBe(false)
    })

    it('ignores whitespace for the same sentence', () => {
      expect(compareSentences('だれのほんですか', 'だれ の ほん です か').correct).toBe(true)
      expect(compareSentences('どうぞたくさんのんでください', 'どうぞ たくさん のんで ください').correct).toBe(true)
    })

    it('matches when it sees an unexpected です', () => {
      expect(compareSentences('６じかん', '６じかんです').correct).toBe(true)
    })

    it('matches when it is missing わたし', () => {
      expect(compareSentences('あした あめ が ふる と おもいます', 'わたしはあしたあめがふるとおもいます').correct).toBe(true)
    })

    it('ignores punctuation', () => {
      expect(compareSentences('a.', 'a').correct).toBe(true)
      expect(compareSentences('a。', 'a').correct).toBe(true)

      expect(compareSentences('a?', 'a').correct).toBe(true)
      expect(compareSentences('a？', 'a').correct).toBe(true)

      expect(compareSentences('a.?', 'a。？').correct).toBe(true)

      expect(compareSentences(
        'いいえ, なら へ いる やくそく が あります',
        'いいえ, なら へ いる, やくそく が あります'
      ).correct).toBe(true)
    })

    it('catches mistakes', () => {
      expect(compareSentences(
        'ここにたばこをすわてないください',
        'ここ に たばこ を すわない で ください'
      ).correct).toBe(false)
    })
  })

  describe('getSentenceQuestionFeedback', () => {
    it('returns null when no feedback exists in the question meta data', () => {
      expect(getSentenceQuestionFeedback(questionWithNoFeedback, '')).toBe(null)
    })

    it('returns null when the question has match feedback, but the answer does not match', () => {
      expect(getSentenceQuestionFeedback(questionWithMatchFeedback, 'なにをかいて？')).toEqual(null)
    })

    it('returns an instance of IQuestionResultFeedback when the question has match feedback', () => {
      expect(getSentenceQuestionFeedback(questionWithMatchFeedback, 'blah blah ないください')).toEqual({
        match: 'ないください',
        advice: '`で` should follow the verb to denote an action'
      })
    })

    it('returns null when the question has missing feedback, but the text exists', () => {
      expect(getSentenceQuestionFeedback(questionWithMissingFeedback, 'じしょう')).toEqual(null)
    })

    it('returns an instance of IQuestionResultFeedback when the question has missing feedback', () => {
      expect(getSentenceQuestionFeedback(questionWithMissingFeedback, 'blah blah')).toEqual({
        missing: 'じしょう',
        advice: 'The word for address is じしょう'
      })
    })
  })

  describe('matchSpacing', () => {
    it('returns a blank string when passed one', () => {
      expect(matchSpacing('', '')).toBe('')
    })

    it('matches a simple case', () => {
      expect(matchSpacing('これはだれですか', 'これ は だれ です か')).toEqual('これ は だれ です か')
    })

    // https://github.com/jhchen/fast-diff

    // みどり ずぼん を はいて いる ひと です
    // みどりずぼんをはいてひとです

    // it('matches an incorrect case', () => {
    //   expect(matchSpacing('これはどれですか', 'これ は だれ です か')).toEqual('これ は だれ です か')
    // })
  })
})
