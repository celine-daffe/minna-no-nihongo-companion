import 'jest'
import * as storage from '../../src/lib/storage'

describe('lib:storage', () => {
  describe('loadOptions', () => {
    beforeEach(() => {
      localStorage.clear()
    })

    it('loads options from localstorage', () => {
      storage.persistOption({
        activityId: 1,
        optionKey: 'SOME_OPTION',
        optionValue: 12
      })

      expect(storage.loadOptions()["1"]).toEqual({
        'NUM_QUESTIONS': 10,
        'SOME_OPTION': 12
      })
    })

    it('loads and correctly groups loaded options', () => {
      storage.persistOption({
        activityId: 1,
        optionKey: 'SOME_OPTION',
        optionValue: 12
      })

      storage.persistOption({
        activityId: 1,
        optionKey: 'SOME_OTHER_OPTION',
        optionValue: 'hello'
      })

      expect(storage.loadOptions()["1"]).toEqual({
        'NUM_QUESTIONS': 10,
        'SOME_OPTION': 12,
        'SOME_OTHER_OPTION': 'hello'
      })
    })

    it('does not duplicate set options, but updates them', () => {
      storage.persistOption({
        activityId: 3,
        optionKey: 'SOME_OPTION',
        optionValue: 12
      })

      storage.persistOption({
        activityId: 3,
        optionKey: 'SOME_OPTION',
        optionValue: 'hello'
      })

      expect(storage.loadOptions()["3"]).toEqual({
        'SOME_OPTION': "hello",
        'NUM_QUESTIONS': 10,
      })
    })
  })

  describe('getOptionsForActivity', () => {
    it('correctly filters by the activity', () => {
      const data = {
        "1": {
          "MAX_CHAPTER": 1,
          "MIN_CHAPTER": 12
        },
        "2": {
          "MIN_CHAPTER": 10,
          "MAX_CHAPTER": 24
        }
      }

      expect(storage.getOptionsForActivity(data, 1)).toEqual({
        "MAX_CHAPTER": 1,
        "MIN_CHAPTER": 12
      })
    })

    it('returns null when no options exist for the activity', () => {
      expect(storage.getOptionsForActivity({}, 1)).toEqual(undefined)
    })
  })
})
