import 'jest'
import {
  getRuleItemType,
  maskGrammarInRuleItem,
  unmaskGrammarInRuleItem,
  RULE_ITEM_TYPE_CONVERSATION,
  RULE_ITEM_TYPE_SENTENCE,
  REPLACEMENT_CHARS,
} from '../../src/lib'

import { IGrammarItem } from '../../src/interfaces'

describe('lib:ruleItem', () => {
  describe('getRuleItemType', () => {
    it('returns 1 for a sentence type', () => {
      const ruleItemFixture: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: '京都 {へ} 行きます'
      }

      expect(getRuleItemType(ruleItemFixture)).toEqual(RULE_ITEM_TYPE_SENTENCE)
    })

    it('returns 2 for a conversation type', () => {
      const ruleItemFixture: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: [
          {
            source: 'A',
            line: 'どこ　へ　いきます　か',
          },
          {
            source: 'B',
            line: 'どこ {も} いきません',
          }
        ]
      }

      expect(getRuleItemType(ruleItemFixture)).toEqual(RULE_ITEM_TYPE_CONVERSATION)
    })
  })

  describe('maskGrammarInRuleItem', () => {
    it('masks for a single particle item', () => {
      const item: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: `コーヒー {の} ほん です`
      }

      expect(maskGrammarInRuleItem(item)).toEqual({
        id: 'abc',
        ruleId: 0,
        hiragana: `コーヒー ${REPLACEMENT_CHARS} ほん です`
      })
    })

    it('returns the original item when no instances of the replacement chars are found', () => {
      const item: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: `こんにちわ`
      }

      expect(maskGrammarInRuleItem(item)).toEqual(item)
    })

    it('masks for a conversation type item', () => {
      const item: IGrammarItem = {
        id: 'LdpFLz3UQ',
        ruleId: 0,
        hiragana: [
          {
            source: 'A',
            line: 'それは　だれの　ほんですか',
          },
          {
            source: 'B',
            line: 'わたし {の} ほんです',
          }
        ]
      }

      expect(maskGrammarInRuleItem(item)).toEqual({
        id: 'LdpFLz3UQ',
        ruleId: 0,
        hiragana: [
          {
            source: 'A',
            line: 'それは　だれの　ほんですか',
          },
          {
            source: 'B',
            line: `わたし ${REPLACEMENT_CHARS} ほんです`,
          }
        ]
      })
    })
  })

  describe('unmaskGrammarInRuleItem', () => {
    it('unmasks for a single particle and tracks what was replaced', () => {
      const item: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: `コーヒー ${REPLACEMENT_CHARS} ほん です`
      }

      expect(unmaskGrammarInRuleItem(['の'], item)).toEqual({
        id: 'abc',
        ruleId: 0,
        hiragana: 'コーヒー の ほん です',
        replaced: [
          'の'
        ]
      })
    })

    it('unmasks in succession for two particles, tracks replacements and ignores them for next iteration', () => {
      const firstItem: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: `かぞく ${REPLACEMENT_CHARS} 日本 ${REPLACEMENT_CHARS} きました`
      }

      expect(unmaskGrammarInRuleItem(['と'], firstItem)).toEqual({
        id: 'abc',
        ruleId: 0,
        hiragana: `かぞく と 日本 ${REPLACEMENT_CHARS} きました`,
        replaced: [
          'と'
        ]
      })

      const secondItem: IGrammarItem = {
        id: 'abc',
        ruleId: 0,
        hiragana: `かぞく と 日本 ${REPLACEMENT_CHARS} きました`,
        replaced: [
          'と'
        ]
      }

      expect(unmaskGrammarInRuleItem(['と', 'へ'], secondItem)).toEqual({
        id: 'abc',
        ruleId: 0,
        hiragana: 'かぞく と 日本 へ きました',
        replaced: [
          'へ'
        ]
      })
    })
  })
})
