import 'jest'
import adjectives from 'minna-no-nihongo-adjectives'
import verbs from 'minna-no-nihongo-verbs'
import * as list from '../../src/lib/list'

describe('lib:list', () => {
  describe('filterCollectionByStringOrChapter', () => {
    it('returns an empty array for an invalid chapter', () => {
      expect(list.filterCollectionByStringOrChapter(verbs, '176')).toEqual([])
      expect(list.filterCollectionByStringOrChapter(verbs, '1')).toEqual([]) // No verbs in chapter 1
    })

    it('filters on an english string with multiple results', () => {
      // work & work overtime
      expect(list.filterCollectionByStringOrChapter(verbs, 'work')).toHaveLength(2)
    })

    it('filters verbs on a hiragana string', () => {
      expect(list.filterCollectionByStringOrChapter(verbs, 'はなします')).toHaveLength(1)
    })

    it('filters adjectives by chapter number', () => {
      expect(
        list.filterAdjectiveCollectionByStringOrChapter(adjectives, '8')
          .length).toBeGreaterThan(0)

      expect(
        list.filterAdjectiveCollectionByStringOrChapter(adjectives, '234')).toHaveLength(0)
    })

    it('filters adjectives by english with multiple results', () => {
      // かんたん & やさしい
      expect(
        list.filterAdjectiveCollectionByStringOrChapter(adjectives, 'easy')
      ).toHaveLength(2)
    })
  })
})