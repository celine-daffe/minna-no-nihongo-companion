import 'jest'
import {
  nextParticles,
  checkAnswer,
  NUMBER_OF_GRAMMAR_ITEM_OPTIONS,
  getGrammarQuizQuestions,
} from '../../src/lib'

import {
  IConversationItem,
  IGrammarItem,
  IGrammarTesterQuestion,
  IQuestion,
} from '../../src/interfaces'

import { Keys } from '../../src/data'

describe('lib:grammar', () => {
  describe('Data Validation', () => {
    it('should have no duplicate ids', () => {
      const ids = getGrammarQuizQuestions({
        NUM_QUESTIONS: 1000,
        MIN_CHAPTER: 1,
        MAX_CHAPTER: 25
      }).questions.map(q => q.id)

      const validate = (): boolean => {
        const foundIds: string[] = []

        ids.map((id: string) => {
          if(foundIds.indexOf(id) > -1){
            throw new Error("Duplicate ID: " + id)
          }

          foundIds.push(id)
        })

        return true
      }

      expect(() => validate()).not.toThrowError()
    })
  })

  describe('nextParticles', () => {
    const ruleItem: IGrammarItem = {
      ruleId: Keys.DEKIMASU_NOUN,
      id: 'LxKiFTue0',
      hiragana: 'Test',
    }

    it('returns a grammar list of the correct size, which includes the desired items', () => {
      const particles = nextParticles(ruleItem)

      expect(particles.length).toEqual(NUMBER_OF_GRAMMAR_ITEM_OPTIONS)
      expect(particles).toContain('の')
    })

    it('returns a grammar list of the correct size, which includes the answer when defined', () => {
      const item = {
        ...ruleItem,
        answer: [
          'から',
          'まで',
          'いいえ'
        ]
      }

      const particles = nextParticles(item)

      expect(particles).toContain('から')
      expect(particles).toContain('まで')
      expect(particles).toContain('いいえ')
    })
  })

  describe('getGrammarQuizQuestions', () => {
    it('returns valid questions', () => {
      const options = {
        NUM_QUESTIONS: 10,
        MIN_CHAPTER: 1,
        MAX_CHAPTER: 25
      }

      const questionItems = getGrammarQuizQuestions(options).questions

      questionItems.map((questionItem: IGrammarTesterQuestion) => {
        expect(questionItem).toHaveProperty('question')
      })
    })

    it('filters by chapter range', () => {
      const options = {
        NUM_QUESTIONS: 4,
        MIN_CHAPTER: 6,
        MAX_CHAPTER: 20
      }

      const questionItems = getGrammarQuizQuestions(options).questions

      questionItems.map((item: IGrammarTesterQuestion) => {
        expect(item.meta.ruleId).toBeGreaterThan(6)
        expect(item.meta.ruleId).toBeLessThan(20)
      })
    })

    it('returns a message when no items were found in the chapter range', () => {
      const options = {
        NUM_QUESTIONS: 4,
        MIN_CHAPTER: 26,
        MAX_CHAPTER: 27
      }

      const { questions, message } = getGrammarQuizQuestions(options)

      expect(questions).toHaveLength(0)
      expect((message as string)).toEqual('No items were found which match the filters provided')
    })

    it('returns a message when the number of returned items is less than the number requested', () => {
      const options = {
        NUM_QUESTIONS: 100,
        MIN_CHAPTER: 14,
        MAX_CHAPTER: 15
      }

      const { questions, message } = getGrammarQuizQuestions(options)

      expect(questions.length).toBeLessThan(100)
      expect((message as string)).toContain('items were found matching the given filters.')
    })

    it('masks particles in a question', () => {
      const options = {
        NUM_QUESTIONS: 10,
        MIN_CHAPTER: 1,
        MAX_CHAPTER: 25
      }

      const questionItem = getGrammarQuizQuestions(options).questions[0]
      const question = questionItem.question as IGrammarItem

      if (typeof question.hiragana === "string") {
        expect(question.hiragana).toContain('<span')
      } else {
        const questionContent = (question.hiragana as IConversationItem[]).map(i => i.line).join(' ')
        expect(questionContent).toContain('<span')
      }
    })
  })

  describe('checkAnswer', () => {
    const question: IQuestion = {
      id: '123',
      answer: ['が', 'を'],
      question: 'hello',
      options: ['で', 'は', 'が', 'を', 'に']
    }

    it('returns a null result when the question.answer is longer than the submitted answer', () => {
      expect(checkAnswer(['が'], question).correct).toBe(null)
    })

    it('returns false when the question.answer and the submitted answer do not match', () => {
      expect(checkAnswer(['が', 'に'], question).correct).toBe(false)
    })

    it('returns true when the question.answer and the submitted answer do not match', () => {
      expect(checkAnswer(['が', 'を'], question).correct).toBe(true)
    })
  })
})
