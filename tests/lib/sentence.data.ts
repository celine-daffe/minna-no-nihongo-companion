import { ISentenceTesterQuestion } from '../../src/interfaces'

export const questionWithNoFeedback: ISentenceTesterQuestion = {
  id: 'mGRCodly6',
  question: 'Some string',
  options: [],
  answer: ['test'],
  meta: {
    rule: {
      id: 6,
      particles: [
        'います'
      ],
      rule: '[V.TeForm] います',
      explanation: 'This pattern indicates that a certain action or motion is in progress'
    },
    sentence: {
      id: 'AU_2O0Gp6',
      ruleId: 14.32,
      hiragana: 'なまえ を かいて ください',
      english: 'Please write your name'
    },
  }
}

export const questionWithMatchFeedback: ISentenceTesterQuestion = {
  id: 'mGRCodly6',
  question: 'ここ に とらない で ください',
  options: [],
  answer: ['test'],
  meta: {
    rule: {
      id: 6,
      particles: [
        'います'
      ],
      rule: '[V.TeForm] います',
      explanation: 'This pattern indicates that a certain action or motion is in progress'
    },
    sentence: {
      id: '71xNu8zIy',
      ruleId: 17.22,
      english: 'Please don\'t {verb} {location}',
      hiragana: '{location} に {verb.NaiForm} で ください',
      feedback: [
        {
          match: 'ます',
          advice: 'Remember to use the ない verb form here'
        },
        {
          match: 'ないください',
          advice: '`で` should follow the verb to denote an action'
        }
      ]
    },
  }
}

export const questionWithMissingFeedback: ISentenceTesterQuestion = {
  id: 'mGRCodly6',
  question: 'ここ に とらない で ください',
  options: [],
  answer: ['test'],
  meta: {
    rule: {
      id: 6,
      particles: [
        'います'
      ],
      rule: '[V.TeForm] います',
      explanation: 'This pattern indicates that a certain action or motion is in progress'
    },
    sentence: {
      id: 'jzjmLR4mX',
      ruleId: 14.32,
      hiragana: 'じしょう を かいて ください',
      english: 'Please write your address',
      feedback: [
        {
          missing: 'じしょう',
          advice: 'The word for address is じしょう'
        }
      ]
    }
  }
}