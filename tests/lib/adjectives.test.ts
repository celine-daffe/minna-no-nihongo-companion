import 'jest'
import { getAdjectiveQuestions } from '../../src/lib'

describe('lib:adjectives', () => {
  describe('getAdjectiveQuestions', () => {
    it('returns the correct number of questions', () => {
      expect(getAdjectiveQuestions({
        NUM_QUESTIONS: 10
      }).questions).toHaveLength(10)
    })

    it('returns questions with no duplicate options', () => {
      const result = getAdjectiveQuestions({
        NUM_QUESTIONS: 20  
      }).questions

      const options = result.map((r => r.options))

      options.map((o) => {
        expect([...new Set(o)].length).toEqual(o.length)
      })
    })
  })
})