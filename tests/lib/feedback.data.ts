import { IDBResult } from '../../src/interfaces'

export const incorrectItemWIthNoFeedback: IDBResult = {
  question_id: 'kizK2gL86',
  results: [
    {
      time: 567489326578432,
      correct: false,
      submittedAnswer: ['blah']
    }
  ]
}

export const correctItemWithFeedback: IDBResult = {
  question_id: 'HTrgTSaIa',
  results: [
    {
      time: 567489326578432,
      correct: true,
      submittedAnswer: ['blah']
    }
  ]
}

export const incorrectItemWithFeedback: IDBResult = {
  question_id: 'kii9dOo2m',
  results: [
    {
      time: 567489326578432,
      correct: false,
      submittedAnswer: ['blah']
    }
  ]
}

export const incorrectItemWithMultipleIdenticalFeedbackItems: IDBResult = {
  question_id: 'kii9dOo2m',
  results: [
    {
      time: 567489326578432,
      correct: false,
      submittedAnswer: ['blah']
    },
    {
      time: 567489326578432,
      correct: false,
      submittedAnswer: ['blah']
    }
  ]
}

export const incorrectItemWithMultipleUniqueFeedbackItems: IDBResult = {
  question_id: 'TkjwXxxl8',
  results: [
    {
      time: 567489326578432,
      correct: false,
      submittedAnswer: ['いる']
    },
    {
      time: 567489326578432,
      correct: false,
      submittedAnswer: ['かぶって']
    }
  ]
}
