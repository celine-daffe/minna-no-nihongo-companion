# minna-no-nihongo-companion

App for self-testing usage of some Japanese grammar and vocabulary based on the Minna no Nihongo textbooks.

Note that this is built for a mobile browser so it looks garbage on a desktop.

https://shandley.gitlab.io/minna-no-nihongo-companion/

## Dev

```
git clone...
npm install
npm start
```

## Goals

- Web based, no app installs on phone
- Offline availability
  - Once data is fetched on first load, does not require remote requests

## TODO

Planned [Features in GitLab](https://gitlab.com/shandley/minna-no-nihongo-companion/issues?label_name%5B%5D=Feature) as well as...

- Complete grammar rules and ensure sentences exist for each, then move out to another package
  - 7
  - 8
  - 9
  - 11
- Handle (more) kanji
- Look into better sentence generation from data placeholders
- In particle cases such as に/へ allow either
- Sliding Transitions during quizzes (useEffect?)
  - Possibly ignore on desktop, but allow on small resolution
- Undo grammar item placement when > 1 entry is available

## Dev fun

- Remove any leftover data from interfaces
- Use Levenshtein distance to accept 'close enough' answers
- Test coverage (particularly async actions)
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/)
- [Commit-lint](https://github.com/conventional-changelog/commitlint)
- Preact - proper PWA

## Bugs

[So Many...](https://gitlab.com/shandley/minna-no-nihongo-companion/issues?label_name%5B%5D=Bug)
